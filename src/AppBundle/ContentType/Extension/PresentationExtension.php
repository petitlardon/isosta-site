<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class PresentationExtension implements ContentTypeExtensionInterface
{
    public function createContentType()
    {
        $ct = new ContentType();
        $ct->setName('Pages présentation');
        $ct->setSlug('presentation');
        $ct->setLinkRewrite('p');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasListView(false);
        $ct->setHasSingleView(true);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(false);

        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);

        $fg = new FieldGroup();
        $fg->setLegend('Thème');
        $fg->setName('theme');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_HIGH);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Titre alternatif (optionnel, affiché si renseigné)');
        $f->setName('page_title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Thème');
        $f->setName('template');
        $f->setFieldType('template_choice');
        $f->setIsRequired(true);
        $f->setDefaultValue('layout_1');
        $fg->addFieldConfig($f);

         $f = new FieldConfig();
        $f->setLabel('Catégorie du contenu');
        $f->setName('category_color');
        $f->setFieldType('sector_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Intro');
        $fg->setName('intro');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('body');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', true);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Image à la une');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Intro');
        $fg->setName('header_site');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Image à la une');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Titre');
        $f->setName('title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Problématique');
        $f->setName('problem');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Solution');
        $f->setName('solution');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Entête');
        $fg->setName('header_slider');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Slider');
        $f->setName('slider');
        $f->setFieldType('slider_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Encart pub');
        $f->setName('advert');
        $f->setFieldType('advert_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Encart pub 2');
        $f->setName('advert_2');
        $f->setFieldType('advert_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $fg = new FieldGroup();
        $fg->setDisplayPane(true);
        $fg->setLegend('Timeline');
        $fg->setName('timeline');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Afficher la timeline sur la page');
        $f->setName('timeline_active');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Contenu externe');
        $fg->setName('addon');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Encart pub');
        $f->setName('advert');
        $f->setFieldType('advert_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Encart pub 2');
        $f->setName('advert_2');
        $f->setFieldType('advert_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Atouts');
        $f->setName('atout');
        $f->setFieldType('atout_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel("Affichage de l'inscription newsletter");
        $f->setName('newsletter');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'content', 'label' => 'Dans le corps du contenu'],
            ['value' => 'sidebar', 'label' => 'Dans la sidebar de droite'],
        ]);
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Redirection vers le formulaire suivant');
        $f->setName('link_form');
        $f->setFieldType('page_form_choice');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $f->setDefaultValue('');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Espace documentaire');
        $f->setName('documentation');
        $f->setFieldType('choice');
        $f->setIsRequired(false);
        $f->setChoices([
            ['value' => 'public', 'label' => 'Partie publique'],
        ]);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Références');
        $fg->setName('references');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Références à afficher');
        $f->setName('reference');
        $f->setFieldType('reference_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Options');
        $fg->setName('security');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Contenu privé');
        $f->setName('is_private');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Gabarit TIM Composites');
        $f->setName('tim');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Bloc de contenu');
        $fg->setName('content');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Type de contenu');
        $f->setName('category');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'presentation', 'label' => 'Présentation avec photo'],
            ['value' => 'category', 'label' => 'Catégorie'],
            ['value' => 'simple_text', 'label' => 'Texte simple'],
        ]);
        $f->setIsRequired(true);
        $f->setDefaultValue('presentation');
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Titre');
        $f->setName('title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Taille');
        $f->setName('size');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'col-md-12', 'label' => 'Toute la largeur'],
            ['value' => 'col-md-6', 'label' => '50 % de la largeur'],
        ]);
        $f->setIsRequired(true);
        $f->setDefaultValue('col-md-12');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Affichage du contenu');
        $f->setName('display');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'col-md-12', 'label' => 'Toute la largeur'],
            ['value' => 'col-md-6', 'label' => '50 % de la largeur'],
        ]);
        $f->setIsRequired(true);
        $f->setDefaultValue('col-md-12');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Photo');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Position de la photo');
        $f->setName('float_picture');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'right', 'label' => 'Droite'],
            ['value' => 'left', 'label' => 'Gauche'],
        ]);
        $f->setIsRequired(true);
        $f->setDefaultValue('right');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('color');
        $f->setFieldType('sector_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur du pictogramme carré');
        $f->setName('square_color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Lien vers la page');
        $f->setName('link');
        $f->setFieldType('page_choice');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $f->setDefaultValue('');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Intitulé du lien');
        $f->setName('link_label');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Photos du chantier');
        $fg->setName('site_photo');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Photo');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $fg = new FieldGroup();
        $fg->setLegend('Catalogue produit');
        $fg->setName('catalogue_produit');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Catalogues');
        $f->setName('catalogues');
        $f->setFieldType('content_link');
        $f->setContentTypeSource('catalogue');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Zoom chantier');
        $fg->setName('site_content');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Surface');
        $f->setName('surface');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Architecte');
        $f->setName('architecte');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Entreprise de pose');
        $f->setName('entreprise');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Matre d\'ouvrage / client');
        $f->setName('client');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Photos');
        $f->setName('photographe');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Période de réalisation du projet');
        $f->setName('date_projet');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Solutions TIM Composite');
        $f->setName('solution');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);
        
        $fg = new FieldGroup();
        $fg->setLegend('Applications');
        $fg->setName('applications');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Applications');
        $f->setName('applications');
        $f->setFieldType('content_link');
        $f->setContentTypeSource('application');
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Partie centrale');
        $fg->setName('center_content');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Partie droite');
        $fg->setName('right_content');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Titre');
        $f->setName('title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Photo');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Légende');
        $f->setName('legend');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Contenu groupe');
        $fg->setName('group_content');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Titre');
        $f->setName('title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur de fond du titre');
        $f->setName('color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Slider');
        $f->setName('slider');
        $f->setFieldType('slider_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Formulaire');
        $fg->setName('form');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Formulaire');
        $f->setName('form');
        $f->setFieldType('form_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Texte alternatif pour les particuliers');
        $f->setName('text_particulier');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', true);
        $fg->addFieldConfig($f);

        return $ct;
    }

    public function getName()
    {
        return 'presentation';
    }
}