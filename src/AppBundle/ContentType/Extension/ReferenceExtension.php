<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class ReferenceExtension implements ContentTypeExtensionInterface
{
    public function createContentType()
    {
        $ct = new ContentType();
        $ct->setName('Références');
        $ct->setSlug('reference');
        $ct->setLinkRewrite('references');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasListView(false);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);

        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
        
        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_HIGH);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Catégorie');
        $f->setName('category_color');
        $f->setFieldType('sector_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);
        
         $f = new FieldConfig();
        $f->setLabel('Lien');
        $f->setName('link-test');
        $f->setFieldType('product_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Références');
        $fg->setName('references');
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Catégorie');
        $f->setName('category');
        $f->setFieldType('reference_category');
        $f->setIsRequired(true);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Image');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Légende');
        $f->setName('legend');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Lien');
        $f->setName('link');
        $f->setFieldType('product_choice');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        return $ct;
    }

    public function getName()
    {
        return 'reference';
    }
}