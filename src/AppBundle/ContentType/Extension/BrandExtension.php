<?php

namespace AppBundle\ContentType\Extension;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;


class BrandExtension implements ContentTypeExtensionInterface {
    public function createContentType() {
        $ct = new ContentType();
        $ct->setName('Marques');
        $ct->setSlug('brand');
        $ct->setLinkRewrite('les-marques');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
            SupportChoice::SUPPORT_THUMBNAIL
        ]);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);
        
        //ajout de contenu perso
        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
         $fg = new FieldGroup();
        $fg->setName('text');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Texte de la marque');
        $f->setName('brand_excerpt');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);

        $fg->addFieldConfig($f);
       
        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);
        
        //ajout champ de formulaire
/*        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);*/
        
        //on veut ce type de champ mais utilisable partout
        //on va faire une conf globale
        //creation d'un ColorType
        
        
        
        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('color');
        $f->setFieldType('sector_color');
        /*$f->setFieldType('choice');
        $f->setChoices([
            ['label' => 'Habitat', 'value' => '#cccc00'],
            ['label' => 'Bâtiment', 'value' => '#dd0022'],
            ['label' => 'Industrie', 'value' => '#ff9955'],
        ]);*/
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);
/******************************************************************************/        
        
        $fg = new FieldGroup();
        $fg->setLegend('Gammes');
        $fg->setName('gamme');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Intitulé de la gamme');
        $f->setName('name');
        $f->setFieldType('text');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Lien vers la page');
        $f->setName('link');
        $f->setFieldType('page_choice');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        return $ct;
    }

    public function getName() {
        return 'brand';
    }

}
