<?php

namespace AppBundle\ContentType\Extension;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;


class AtoutExtension implements ContentTypeExtensionInterface {
    public function createContentType() {
        $ct = new ContentType();
        $ct->setName('Atouts');
        $ct->setSlug('atout');
        $ct->setLinkRewrite('les-atouts');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);
        
        //ajout de contenu perso
        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
        
        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('color');
        $f->setFieldType('sector_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);

/******************************************************************************/        
        
        $fg = new FieldGroup();
        $fg->setLegend('Descriptif');
        $fg->setName('descriptif');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Intitulé de l\'atout');
        $f->setName('atout_title');
        $f->setFieldType('text');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
                
        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('atout_text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        return $ct;
    }

    public function getName() {
        return 'atout';
    }

}
