<?php

namespace AppBundle\ContentType\Extension;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;


class ApplicationExtension implements ContentTypeExtensionInterface {
    public function createContentType() {
        $ct = new ContentType();
        $ct->setName('Applications');
        $ct->setSlug('application');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasSingleView(false);
        $ct->setHasListView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);
        
        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
         $fg = new FieldGroup();
        $fg->setName('text');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);
               
        $fg = new FieldGroup();
        $fg->setLegend('Applications');
        $fg->setName('application');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(false);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Catégorie');
        $f->setName('category');
        $f->setFieldType('application_choice');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Libellé');
        $f->setName('libelle');
        $f->setFieldType('text');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        return $ct;
    }

    public function getName() {
        return 'application';
    }

}
