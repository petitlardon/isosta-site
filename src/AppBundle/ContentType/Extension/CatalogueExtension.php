<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class CatalogueExtension implements ContentTypeExtensionInterface
{
    public function createContentType()
    {
        $ct = new ContentType();
        $ct->setName('Catalogue');
        $ct->setSlug('catalogue');
        $ct->setLinkRewrite('catalogue');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasListView(false);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);

        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);

        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_HIGH);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('category_color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Catégorie');
        $f->setName('category');
        $f->setFieldType('text');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Produits');
        $fg->setName('products');
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Image');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Description');
        $f->setName('description');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $fg->addFieldConfig($f);

        return $ct;
    }

    public function getName()
    {
        return 'catalogue';
    }
}
