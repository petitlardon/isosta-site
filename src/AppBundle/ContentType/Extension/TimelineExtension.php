<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class TimelineExtension implements ContentTypeExtensionInterface
{
    public function getName()
    {
        return 'timeline';
    }

    public function createContentType()
    {
        $contentType = new ContentType();
        $contentType->setName('Timeline');
        $contentType->setSlug('timeline');
        $contentType->setLinkRewrite('timeline');
        $contentType->setSupports(array(
            SupportChoice::SUPPORT_TITLE,
        ));
        $contentType->setHasAdminView(true);
        $contentType->setHasListView(false);
        $contentType->setHasArchiveView(false);
        $contentType->setHasSingleView(false);
        $contentType->setHasCommentView(false);
        $contentType->setIsExcludedFromGlobalSearch(true);
        $contentType->setMaxPerPage(0);

        $embeddedForm = new EmbeddedForm();
        $contentType->setEmbeddedForm($embeddedForm);

        $fg = new FieldGroup();
        $fg->setDisplayPane(false);
        $fg->setLegend('Contenu');
        $fg->setName('content');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Date');
        $f->setName('date');
        $f->setFieldType('date');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_SMALL);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setDisplayPane(true);
        $fg->setLegend('Les activités de groupe');
        $fg->setName('activite');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Photo');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Texte (180 caractères max)');
        $f->setName('body');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', true);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Couleur de fond (au survol)');
        $f->setName('background_color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur du texte');
        $f->setName('text_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $f->setDefaultValue('#ffffff');
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setDisplayPane(true);
        $fg->setLegend('Les produits');
        $fg->setName('produit');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Photo');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Texte (180 caractères max)');
        $f->setName('body');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', true);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Couleur de fond (au survol)');
        $f->setName('background_color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur du texte');
        $f->setName('text_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $f->setDefaultValue('#ffffff');
        $fg->addFieldConfig($f);

        return $contentType;
    }
}
