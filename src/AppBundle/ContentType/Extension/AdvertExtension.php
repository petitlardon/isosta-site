<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class AdvertExtension implements ContentTypeExtensionInterface
{
    public function createContentType()
    {
        $ct = new ContentType();
        $ct->setName('Encarts PUB');
        $ct->setSlug('advert');
        $ct->setLinkRewrite('pubs');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
            SupportChoice::SUPPORT_THUMBNAIL,
        ]);
        $ct->setHasListView(false);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);

        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);

        $fg = new FieldGroup();
        $fg->setLegend('Options');
        $fg->setName('options');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $fg->setMayBeRepeated(false);
        $fg->setDisplayPane(true);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Afficher dans l\'espace documentaire');
        $f->setName('is_storage');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel("Afficher uniquement l'image");
        $f->setName('image_only');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel("Rediriger vers les références dans la page");
        $f->setName('redirect_ref');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Lien vers la page');
        $f->setName('link');
        $f->setFieldType('page_choice');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $f->setDefaultValue('');
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Intitulé du lien');
        $f->setName('link_label');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);

         $f = new FieldConfig();
        $f->setLabel('Couleur de fond de la pub');
        $f->setName('background_color_charte');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

         $f = new FieldConfig();
        $f->setLabel(' ');
        $f->setName('background_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

         $f = new FieldConfig();
        $f->setLabel('Couleur de fond des éléments');
        $f->setName('button_background_color_charte');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

         $f = new FieldConfig();
        $f->setLabel(' ');
        $f->setName('button_background_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

         $f = new FieldConfig();
        $f->setLabel('Couleur du contenu');
        $f->setName('fields_color_charte');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
         $f = new FieldConfig();
        $f->setLabel(' ');
        $f->setName('fields_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Texte');
        $fg->setName('text');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);
         
        $f = new FieldConfig();
        $f->setLabel('Titre à afficher (optionnel)');
        $f->setName('advert_title');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $f->setDefaultValue('');
        $fg->addFieldConfig($f);
       
       $f = new FieldConfig();
        $f->setLabel('Texte de l\'encart');
        $f->setName('body');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);        
        


        return $ct;
    }

    public function getName()
    {
        return 'advert';
    }
}