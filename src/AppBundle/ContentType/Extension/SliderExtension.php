<?php

namespace AppBundle\ContentType\Extension;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

class SliderExtension implements ContentTypeExtensionInterface
{
    public function createContentType()
    {
        $ct = new ContentType();
        $ct->setName('Carousels');
        $ct->setSlug('slider');
        $ct->setLinkRewrite('carousels');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
        ]);
        $ct->setHasListView(false);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);

        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
        
        $fg = new FieldGroup();
        $fg->setLegend('Options');
        $fg->setName('options');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $fg->setMayBeRepeated(false);
        $fg->setDisplayPane(true);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Afficher en page d\'accueil');
        $f->setName('is_home');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Afficher dans l\'espace documentaire');
        $f->setName('is_storage');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $fg = new FieldGroup();
        $fg->setLegend('Slides');
        $fg->setName('slides');
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);

        $f = new FieldConfig();
        $f->setLabel('Texte');
        $f->setName('text');
        $f->setFieldType('editor');
        $f->setIsRequired(false);
        $f->setOption('toolbar', false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Insérer le texte en dessous du slider');
        $f->setName('bottom_text');
        $f->setFieldType('checkbox');
        $f->setIsRequired(false);
        $f->setDefaultValue(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Couleur du texte');
        $f->setName('text_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur de fond');
        $f->setName('slide_background_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Position du texte');
        $f->setName('alignement');
        $f->setFieldType('choice');
        $f->setChoices([
            ['value' => 'left', 'label' => 'Gauche'],
            ['value' => 'center', 'label' => 'Centré'],
            ['value' => 'right', 'label' => 'Droit'],
        ]);
        $f->setIsRequired(true);
        $f->setDefaultValue('center');
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Intitulé du lien');
        $f->setName('link_label');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Lien');
        $f->setName('link');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Couleur de fond du lien');
        $f->setName('background_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Image');
        $f->setName('picture');
        $f->setFieldType('image_library');
        $f->setIsRequired(false);

        $fg->addFieldConfig($f);

        return $ct;
    }

    public function getName()
    {
        return 'slider';
    }
}