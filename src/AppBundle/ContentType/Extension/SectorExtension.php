<?php

namespace AppBundle\ContentType\Extension;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeExtensionInterface;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\ContentType\Choice\DisplaySizeChoice;


class SectorExtension implements ContentTypeExtensionInterface {
    public function createContentType() {
        $ct = new ContentType();
        $ct->setName('Secteurs');
        $ct->setSlug('sector');
        $ct->setLinkRewrite('les-secteurs');
        $ct->setSupports([
            SupportChoice::SUPPORT_TITLE,
            SupportChoice::SUPPORT_THUMBNAIL
        ]);
        $ct->setHasSingleView(false);
        $ct->setMaxPerPage(0);
        $ct->setIsExcludedFromGlobalSearch(true);
        
        //ajout de contenu perso
        $embeddedForm = new EmbeddedForm();
        $ct->setEmbeddedForm($embeddedForm);
        
        $fg = new FieldGroup();
        $fg->setLegend('Configuration');
        $fg->setName('configuration');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_SIDEBAR);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Couleur');
        $f->setName('color');
        $f->setFieldType('sector_color');
        $f->setIsRequired(true);
        $fg->addFieldConfig($f);

        $f = new FieldConfig();
        $f->setLabel('Couleur du bouton');
        $f->setName('button_color');
        $f->setFieldType('color_picker');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);
/******************************************************************************/        
        
        $fg = new FieldGroup();
        $fg->setLegend('Gammes');
        $fg->setName('gamme');
        $fg->setDisplayPane(true);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(true);
        $fg->setMayBeRepeated(true);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Intitulé de la gamme');
        $f->setName('name');
        $f->setFieldType('text');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Lien vers la page');
        $f->setName('link');
        $f->setFieldType('page_choice');
        $f->setIsRequired(true);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        
        
        $fg = new FieldGroup();
        $fg->setLegend('Bouton');
        $fg->setName('button');
        $fg->setDisplayPane(false);
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $fg->setMayBeSorted(false);
        $fg->setMayBeRepeated(false);
        $embeddedForm->addFieldGroup($fg);
        
        $f = new FieldConfig();
        $f->setLabel('Bouton de redirection');
        $f->setName('button_label');
        $f->setFieldType('text');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        $f = new FieldConfig();
        $f->setLabel('Lien vers la page');
        $f->setName('button_link');
        $f->setFieldType('page_choice');
        $f->setIsRequired(false);
        $f->setDisplaySize(DisplaySizeChoice::DISPLAY_SIZE_LARGE);
        $fg->addFieldConfig($f);
        
        return $ct;
    }

    public function getName() {
        return 'sector';
    }

}
