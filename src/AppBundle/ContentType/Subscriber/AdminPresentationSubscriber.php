<?php

namespace AppBundle\ContentType\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\CoreBundle\DataGrid\DataGridEvents;
use Symfony\Component\EventDispatcher\GenericEvent;
use Ines\Bundle\CoreBundle\DataGrid\DataGridColumn;
use Doctrine\ORM\EntityManager;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeEvents;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Ines\Bundle\CoreBundle\ContentType\Event\CreateQueryBuilderEvent;

/**
 * Description of AdminPresentationSubscriber
 *
 * @author Web
 */

class AdminPresentationSubscriber implements EventSubscriberInterface {
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public static function getSubscribedEvents() {
        //quel evenement on écoute et quelle méthode executée
        return [
            sprintf(ContentTypeEvents::FILTER_FORM_LISTENER, 'presentation') => 'onFilterFormListener',
            sprintf(ContentTypeEvents::CREATE_QUERYBUILDER, 'presentation') => 'onCreateQueryBuilder',
        ];        
    }
        
    public function onFilterFormListener(GenericEvent $event) {
        //liste de tous les ecouteurs de formulaire disponible
        $listeners = $event->getSubject();
        $listeners->add([
            //PRE_SET_DATA juste avant la créationd e formulaire
            'event' => FormEvents::PRE_SET_DATA,
            'callback' => function(FormEvent $event) {
                $form = $event->getForm();
                //dump($form);die;
                $form->add('template', 'template_choice', [
                    'attr' => ['placeholder' => 'Template' ],
                    'empty_value' => 'Template',
                    'required' => false,  
                ]);
            }
        ]);
        //ca ne fait pas d'erreur mais ne fonctionne pas. il faut implémenter la requete (nouvel écouteur)
    }
    
    public function onCreateQueryBuilder(CreateQueryBuilderEvent $event) {
        $qb = $event->getQueryBuilder();
        $filters = $event->getFilters();
        
        if(empty($filters['template'])) {
            return;
        }
        //comme on est en EAV, deux param a recup meta_key + meta_value
        
        //premier cas simple uniquement si une colonne perso pour eviter els effets de bord
        /*$qb
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->andWhere('cm.metaValue = :color')
                ->setParameter('color', $filters['color']);*/
        
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'template')
                ->andWhere('cm.metaValue = :template')
                ->setParameter('template', $filters['template'])
                ->groupBy('c.id')
            ;
        $ids = $qb1->getQuery()->getScalarResult();
        $ids = array_column($ids, 'id');
        //dump($ids);die;
        if(!$ids) {
            return;
        }
        
        $qb
                ->andWhere($qb->expr()->in('c.id', $ids));
    }
}
