<?php

namespace AppBundle\ContentType\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\CoreBundle\DataGrid\DataGridEvents;
use Symfony\Component\EventDispatcher\GenericEvent;
use Ines\Bundle\CoreBundle\DataGrid\DataGridColumn;
use Doctrine\ORM\EntityManager;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeEvents;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Ines\Bundle\CoreBundle\ContentType\Event\CreateQueryBuilderEvent;

/**
 * Description of AdminBrandSubscriber
 *
 * @author Web
 */

class AdminSliderSubscriber implements EventSubscriberInterface {
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public static function getSubscribedEvents() {
        //quel evenement on écoute et quelle méthode executée
        return [
            DataGridEvents::GET_COLUMNS => 'onGetColumns',
            sprintf(ContentTypeEvents::FILTER_FORM_LISTENER, 'slider') => 'onFilterFormListener',
            sprintf(ContentTypeEvents::CREATE_QUERYBUILDER, 'slider') => 'onCreateQueryBuilder',
        ];        
    }
    
    public function onGetColumns(GenericEvent $event) {
        $columns = $event->getSubject();
        $config = $event->getArgument('config');
        if ('slider' !== $config->getContentType()->getSlug()) {
            return;
        }
        //source est notre content. pas déclaré car généré à la volée
        $columns->add(new DataGridColumn([
            'label' => 'Page d\'accueil',
            'value_callback' => function($source, $options) {
                return $source->getContentMetaValue('is_home');
            },
            'raw_value' => true,
            'format_value' => function($value, $source, $option) {
                if(!$value) {
                    return '';
                }
                return sprintf('<div style="width: 25px; height: 25px; ">%s</div>', $value);
            },
                    'extra'
            //la value = $source->getContentMetaValue car non multiple sinon tableau
        ]));
            
    }
    
    public function onFilterFormListener(GenericEvent $event) {

        }
    
    public function onCreateQueryBuilder(CreateQueryBuilderEvent $event) {
        $qb = $event->getQueryBuilder();
        $filters = $event->getFilters();
        
        if(empty($filters['is_home'])) {
            return;
        }
        //comme on est en EAV, deux param a recup meta_key + meta_value
        
        //premier cas simple uniquement si une colonne perso pour eviter els effets de bord
        /*$qb
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->andWhere('cm.metaValue = :color')
                ->setParameter('color', $filters['color']);*/
        
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'is_home')
                ->andWhere('cm.metaValue = :is_home')
                ->setParameter('is_home', $filters['is_home'])
                ->groupBy('c.id')
            ;
        $ids = $qb1->getQuery()->getScalarResult();
        $ids = array_column($ids, 'id');
        if(!$ids) {
            return;
        }
        
        $qb
                ->andWhere($qb->expr()->in('c.id', $ids));
    }
}
