<?php

namespace AppBundle\ContentType\Subscriber;

use Ines\Bundle\CoreBundle\ContentType\Choice\DisplayPositionChoice;
use Ines\Bundle\CoreBundle\ContentType\Choice\SupportChoice;
use Ines\Bundle\CoreBundle\ContentType\Event\CreateContentTypeEvent;
use Ines\Bundle\CoreBundle\Entity\EmbeddedForm;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Ines\Bundle\CoreBundle\Entity\FieldGroup;

/**
 * Description of PostExtensionListener
 *
 * @author Web
 */
class PostExtensionListener {
    public function onPreRegister(CreateContentTypeEvent $event) {
        $contentType = $event->getContentType();

        $embeddedForm = new EmbeddedForm();
        $contentType->setEmbeddedForm($embeddedForm);
        
        $fg = new FieldGroup();
        $fg->setDisplayPane(false);
        $fg->setLegend('Options');
        $fg->setName('option');
        $fg->setDisplayPosition(DisplayPositionChoice::DISPLAY_POSITION_NORMAL);
        $embeddedForm->addFieldGroup($fg);
        
         $f = new FieldConfig();
        $f->setLabel("Couleur de l'actualité");
        $f->setName('charte_color');
        $f->setFieldType('charte_color');
        $f->setIsRequired(false);
        $fg->addFieldConfig($f);

    }
}
