<?php

namespace AppBundle\ContentType\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\CoreBundle\DataGrid\DataGridEvents;
use Symfony\Component\EventDispatcher\GenericEvent;
use Ines\Bundle\CoreBundle\DataGrid\DataGridColumn;
use Doctrine\ORM\EntityManager;
use Ines\Bundle\CoreBundle\ContentType\ContentTypeEvents;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Ines\Bundle\CoreBundle\ContentType\Event\CreateQueryBuilderEvent;

/**
 * Description of AdminBrandSubscriber
 *
 * @author Web
 */

class AdminBrandSubscriber implements EventSubscriberInterface {
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public static function getSubscribedEvents() {
        //quel evenement on écoute et quelle méthode executée
        return [
            DataGridEvents::GET_COLUMNS => 'onGetColumns',
            sprintf(ContentTypeEvents::FILTER_FORM_LISTENER, 'brand') => 'onFilterFormListener',
            sprintf(ContentTypeEvents::CREATE_QUERYBUILDER, 'brand') => 'onCreateQueryBuilder',
        ];        
    }
    
    public function onGetColumns(GenericEvent $event) {
        $columns = $event->getSubject();
        $config = $event->getArgument('config');
        if ('brand' !== $config->getContentType()->getSlug()) {
            return;
        }
        //source est notre content. pas déclaré car généré à la volée
        $columns->add(new DataGridColumn([
            'label' => 'Secteur',
            'value_callback' => function($source, $options) {
                //dump($source);die;
                return $source->getContentMetaValue('color');
            },
            'raw_value' => true,
            'format_value' => function($value, $source, $option) {
                if(!$value) {
                    return '';
                }
                return sprintf('<div style="width: 25px; height: 25px; background-color: %s"></div>', $value);
            },
                    'extra'
            //la value = $source->getContentMetaValue car non multiple sinon tableau
        ]));
            
        $columns->add(new DataGridColumn([
            'label' => 'Contenu lié',
            'value_callback' => function($source, $options) {
                $id = $source->getContentMetaValue('id_lie');
                if(!$id) {
                    return null;
                }
                $em = $options['em'];
                //dump($source);die;
                return $em->getRepository('InesCoreBundle:Content')->find($id);
            },
            'raw_value' => true,
            'format_value' => function($value, $source, $option) {
                if(!$value) {
                    return '';
                }
                return $value->getTitle();
            },
            'extra' => [
                'em' => $this->em,
            ],
            //la value = $source->getContentMetaValue car non multiple sinon tableau
        ]));

    }
    
    public function onFilterFormListener(GenericEvent $event) {
        //liste de tous les ecouteurs de formulaire disponible
        $listeners = $event->getSubject();
        $listeners->add([
            //PRE_SET_DATA juste avant la créationd e formulaire
            'event' => FormEvents::PRE_SET_DATA,
            'callback' => function(FormEvent $event) {
                $form = $event->getForm();
                $form->add('color', 'sector_color', [
                    'attr' => ['placeholder' => 'Couleur' ],
                    'empty_value' => 'Couleur',
                    'required' => false,  
                ]);
            }
        ]);
        //ca ne fait pas d'erreur mais ne fonctionne pas. il faut implémenter la requete (nouvel écouteur)
    }
    
    public function onCreateQueryBuilder(CreateQueryBuilderEvent $event) {
        $qb = $event->getQueryBuilder();
        $filters = $event->getFilters();
        
        if(empty($filters['color'])) {
            return;
        }
        //comme on est en EAV, deux param a recup meta_key + meta_value
        
        //premier cas simple uniquement si une colonne perso pour eviter els effets de bord
        /*$qb
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->andWhere('cm.metaValue = :color')
                ->setParameter('color', $filters['color']);*/
        
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')
                ->innerJoin('c.contentMetas', 'cm')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->andWhere('cm.metaValue = :color')
                ->setParameter('color', $filters['color'])
                ->groupBy('c.id')
            ;
        $ids = $qb1->getQuery()->getScalarResult();
        $ids = array_column($ids, 'id');
        //dump($ids);die;
        if(!$ids) {
            return;
        }
        
        $qb
                ->andWhere($qb->expr()->in('c.id', $ids));
    }
}
