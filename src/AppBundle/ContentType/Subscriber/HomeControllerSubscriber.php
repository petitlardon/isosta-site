<?php

namespace AppBundle\ContentType\Subscriber;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use Ines\Bundle\ThemeBundle\Event\RenderEvent;
use Ines\Bundle\ThemeBundle\ThemeEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class HomeControllerSubscriber implements EventSubscriberInterface
{
    protected $em;

    protected $requestStack;

    public function __construct(EntityManager $em,
        RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public static function getSubscribedEvents()
    {
        return [
            ThemeEvents::PRE_RENDER_HOME => 'onPreRenderHome',
        ];
    }

    public function onPreRenderHome(RenderEvent $event)
    {
        $request = $this->requestStack->getMasterRequest();
        if (!$request) {
            return;
        }

        $viewArg = $event->getViewArgument();

        // recherche du slider activé pour la page d'accueil
        $sliderContentType = ContentTypeManager::fetchOne('slider');
        $qb = $this->em->getRepository('InesCoreBundle:Content')
            ->getFrontListQueryBuilder($request->getLocale(), $sliderContentType)
            ->select('DISTINCT(c.id) as id')
            ->andWhere('cm.metaKey = :home AND cm.metaValue = :true')
            ->setParameter('home', 'is_home')
            ->setParameter('true', true)
            ->setMaxResults(1)
        ;

        try {
            $sliderId = (int) $qb->getQuery()->getSingleScalarResult();
        } catch (NoResultException $e) {
            $sliderId = null;
        }

        $viewArg['slider_id'] = $sliderId;
    }
}
