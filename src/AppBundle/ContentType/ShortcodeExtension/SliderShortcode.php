<?php

namespace AppBundle\ContentType\ShortcodeExtension;

use Doctrine\ORM\EntityManager;
use Ines\Bundle\CoreBundle\ContentType\Choice\StatusChoice;
use Ines\Bundle\CoreBundle\ContentType\Shortcode\ShortcodeInterface;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use Ines\Bundle\ThemeBundle\Facade\TemplateTheme;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SliderShortcode implements ShortcodeInterface
{
    protected $em;

    protected $requestStack;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public function getTag()
    {
        return 'slider';
    }

    public function resolveAttributes($atts = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired([
            'id',
        ]);

        $resolver->setAllowedTypes([
            'id' => 'numeric',
        ]);

        return $resolver->resolve($atts);
    }

    public function doRender($attrs, $content = null, $tag)
    {
        $id = (int) $attrs['id'];
        $request = $this->requestStack->getMasterRequest();
        $sliderContentType = ContentTypeManager::fetchOne('slider');

        $qb = $this->getContentRepository()
            ->getListQueryBuilder($request->getLocale(), $sliderContentType);
        $qb
            ->select(array('c', 'cm'))
            ->leftJoin('c.contentMetas', 'cm')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->andWhere('c.status = :status')
            ->setParameter('status', StatusChoice::STATUS_PUBLISH)
        ;

        $slider = $qb->getQuery()->getOneOrNullResult();
        if (!$slider) {
            return;
        }

        // filter unpublished items
        $items = $slider->getContentMetasGroup('items', true, true);
        if (!$items) {
            return;
        }

        $items = array_filter($items, function($item) {
            $started = $item['started_at'];
            $ended = $item['ended_at'];
            $now = new \DateTime();

            if (($date = $started->getMetaValue())
            && $now < $date) {
                return false;
            }

            if (($date = $ended->getMetaValue())
            && $now > $date) {
                return false;
            }

            return true;
        });

        // extract medias
        $mediaIds = array_map(function($item) {
            $picture = $item['picture'];
            return (int) $picture->getMetaValue();
        }, $items);

        $medias = $this->getMediaRepository()->findBy(['id' => $mediaIds]);

        // reformat array with contentMetaValue
        // and media
        $items = array_map(function($item) use ($medias) {
            $item = array_map(function($meta) {
                return $meta->getMetaValue();
            }, $item);

            $filteredMedias = array_filter($medias, function($media) use($item) {
                return $media->getId() == $item['picture'];
            });

            $item['picture'] = $filteredMedias ? current($filteredMedias) : null;
            return $item;
        }, $items);
//dump($items);die;
        return TemplateTheme::renderShortcode($this, [
            'items' => $items,
        ]);
    }

    protected function getContentRepository()
    {
        return $this->em->getRepository('InesCoreBundle:Content');
    }

    protected function getMediaRepository()
    {
        return $this->em->getRepository('InesCoreBundle:Media');
    }
}
