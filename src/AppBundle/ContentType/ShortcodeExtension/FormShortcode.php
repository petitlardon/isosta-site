<?php

namespace AppBundle\ContentType\ShortcodeExtension;

use Doctrine\ORM\EntityManager;
use Ines\Bundle\CoreBundle\ContentType\Shortcode\ShortcodeInterface;
use Ines\Bundle\CoreBundle\Form\Type\WebFormEntryType;
use Ines\Bundle\ThemeBundle\Facade\TemplateTheme;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormShortcode implements ShortcodeInterface
{
    protected $em;

    protected $formFactory;

    public function __construct(EntityManager $em, FormFactoryInterface $formFactory)
    {
        $this->em = $em;
        $this->formFactory = $formFactory;
        
    }

    public function doRender($atts, $content = null, $tag)
    {
        $id = (int) $atts['id'];
        $webForm = $this->getWebFormRepository()->fetchOneActive($id);
        if (!$webForm) {
            return;
        }

        if ($webForm->getLimitEntries()) {
            // see if entry count is less then specified limit
            $count = $this->getContentRepository()->countWebFormEntry($webForm);
            if ((int) $count >= (int) $webForm->getLimitEntries()) {
                return;
            }
        }

        $form = $this->formFactory->create(new WebFormEntryType(), null, [
            'method'   => 'POST',
            'web_form' => $webForm,
        ]);

        return TemplateTheme::renderShortcode($this, [
            'form'    => $form->createView(),
            'webForm' => $webForm,
        ]);
    }

    public function getTag()
    {
        return 'form';
    }

    public function resolveAttributes($atts = [])
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired([
            'id',
        ]);

        $resolver->setAllowedTypes([
            'id' => 'numeric',
        ]);

        return $resolver->resolve($atts);
    }

    protected function getContentRepository()
    {
        return $this->em->getRepository('InesCoreBundle:Content');
    }

    protected function getWebFormRepository()
    {
        return $this->em->getRepository('InesCoreBundle:WebForm');
    }
}
