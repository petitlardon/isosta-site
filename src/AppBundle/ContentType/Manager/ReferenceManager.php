<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use AppBundle\Form\Type\SectorColorType;

/**
 * Description of BrandManager
 *
 * @author Web
 */
class ReferenceManager {
    protected $em;
    protected $requestStack;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    public function createListQueryBuilder() {
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('reference');
        
        //récupération d'un query builder
        return $repository->getFrontListQueryBuilder($locale, $contentType);
    }
    
    public function applySort(QueryBuilder $qb) {
                //récupération des valeurs de couleur dans l'ordre
        //comme c du string on protege le string
        //$colors = array_keys(SectorColorType::$choices);
        
        //arraymap
        $colors = array_map(function($color) use ($qb) {
            return $qb->expr()->literal($color);
        }, array_keys(SectorColorType::$choices));
        $qb1 = clone $qb;
        $qb1
                ->select('c.id, FIELD(cm.metaValue, '. implode(',', $colors).') AS HIDDEN field')->groupBy('c.id')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->orderBy('field');
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        if($ids) {
            $qb
                    ->addSelect('FIELD(c.id, '. implode(',', $ids).') AS HIDDEN field')
                    ->orderBy('field');
        }
    }
    
    public function fetchAll() {
        $qb = $this->createListQueryBuilder();
        $this->applySort($qb);
        
        return $qb->getQuery()->getResult();//on déclare le service manager
    }
}
