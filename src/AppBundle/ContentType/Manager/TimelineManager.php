<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;

/**
 * Description of TimelineManager
 *
 * @author Web
 */
class TimelineManager {
    protected $em;
    protected $requestStack;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    public function createListQueryBuilder() {
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('timeline');
        
        //récupération d'un query builder
        return $repository->getFrontListQueryBuilder($locale, $contentType);
    }
    
    public function applySort(QueryBuilder $qb) {       
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')->groupBy('c.id')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'date')
                ->orderBy('cm.metaValue')
                ;
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        if($ids) {
            $qb
                    ->addSelect('FIELD(c.id, '. implode(',', $ids).') AS HIDDEN field')
                    ->orderBy('field');
        }
        $qb->andWhere($qb->expr()->in('c.id', $ids));
    }
    
    public function fetchAll() {
        $qb = $this->createListQueryBuilder();
        
        $this->applySort($qb);
        return $qb->getQuery()->getResult();//on déclare le service manager
    }
    

}
