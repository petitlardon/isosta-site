<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use AppBundle\Form\Type\SectorColorType;

/**
 * Description of SliderManager
 *
 * @author Web
 */
class SliderManager {
    protected $em;
    protected $requestStack;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    public function createListQueryBuilder() {
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('slider');
        
        //récupération d'un query builder
        return $repository->getFrontListQueryBuilder($locale, $contentType);
    }
    
    public function applySort(QueryBuilder $qb, $metakey) {       
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')->groupBy('c.id')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', $metakey)
                ->andWhere('cm.metaValue = :value')
                ->setParameter('value', '1')
                ;
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        if($ids) {
            $qb
                    ->addSelect('FIELD(c.id, '. implode(',', $ids).') AS HIDDEN field')
                    ->orderBy('field');
        }
        $qb->andWhere($qb->expr()->in('c.id', $ids));
    }
    
    public function fetchOneByMetakey($metakey) {
        $qb = $this->createListQueryBuilder();
        
        $this->applySort($qb, $metakey);
        return $qb->getQuery()->getResult();//on déclare le service manager
    }
    

}
