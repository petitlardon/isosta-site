<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use Ines\Bundle\CoreBundle\Facade\ContentTypeRegistry;
use AppBundle\Form\Type\SectorColorType;
use Symfony\Component\Form\FormFactoryInterface;
use Ines\Bundle\CoreBundle\Form\Type\WebFormEntryType;
use Ines\Bundle\CoreBundle\Facade\ListView;

/**
 * Description of BrandManager
 *
 * @author Web
 */
class PresentationManager {
    protected $em;
    protected $requestStack;
    protected $formFactory;
    
    public function __construct(EntityManager $em, RequestStack $requestStack, FormFactoryInterface $formFactory) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
    }
    
    public function createListQueryBuilder() {
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('presentation');
        
        //récupération d'un query builder
        return $repository->getFrontListQueryBuilder($locale, $contentType);
    }
    
    public function applySort(QueryBuilder $qb) {

        $qb1 = clone $qb;
        $qb1
                ->select('c.id')->groupBy('c.id')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'reference');
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        $model = $qb1->getQuery()->getOneOrNullResult();
        if(!$ids) {
            return;
        }
        //ListView::buildView($ids);
        $qb
                ->andWhere($qb->expr()->in('c.id', $ids));
        //$models[$productLine->getId()] = $model;
        //dump($model);die;
        
    }
    
    public function fetchAll() {
        $qb = $this->createListQueryBuilder();
        $this->applySort($qb);
        
        return $qb->getQuery()->getResult();//on déclare le service manager
    }
        
    public function fetchReferences($entry, $args) {
        $contentType = ContentTypeRegistry::get('reference');
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        $request = $this->requestStack->getMasterRequest();
        
        $qb = $repository->getFrontListQueryBuilder($request->getLocale(), $contentType);
            $qb
                ->select('c')
                ->andWhere('c.id = :id')
                ->setParameter('id', $entry->reference->id)
            ;
//dump($qb);die;
            $model = $qb->getQuery()->getResult();
            $models[$entry->reference->id] = $model;
        
        dump($qb);dump($args);dump($entry);dump($model);dump($models);die;
        
        
        
        
        
        $modelContentType = ContentTypeRegistry::get('model');
        $args['modelContentType'] = $modelContentType;
dump($modelContentType);die;
        $models = [];
        //dump($args);
        foreach ($entry->reference->metas->references as $reference) {
            $qb = $repository->getFrontListQueryBuilder($request->getLocale(), $modelContentType);
            $qb
                ->select('c')
                ->andWhere('cm.format = :format and cm.metaValue LIKE :meta_value')
                ->setParameter('format', 'content_link')
                ->setParameter('meta_value', '%|'.$productLine->getId().'|%')
                ->andWhere('cm2.metaKey = :meta_key')
                ->setParameter('meta_key', 'position')
                ->orderBy('cm2.metaValue', 'ASC')
                ->setMaxResults(1)
            ;

            $model = $qb->getQuery()->getOneOrNullResult();
            $models[$reference->getId()] = $model;
            dump($models);die;
            ListView::buildView($reference);
        }
        $request = $this->requestStack->getMasterRequest();

        $repository = $this->em->getRepository('InesCoreBundle:Content');
        $productLines = ContentTypeRegistry::get('reference');
        //$productLines = $repository->getFrontLinkedContents($entry->configuration->source, $productLineContentType, $request->getLocale());

        dump($entry);dump($args);dump($productLines);die;
        if (!$entry) {
            return;
        }

        // Tri par position ascendent
        uasort($productLines, function($a, $b) {
            $positionA = $a->getContentMetaValue('position');
            $positionB = $b->getContentMetaValue('position');

            if ($positionA == $positionB) {
                return 0;
            }

            return ($positionA < $positionB) ? -1 : 1;
        });

        // Utilisation de ListView pour récupérer automatiquement les médias associés
        $args['productLines'] = ListView::buildView($productLines);

        // Récupération du 1er modèle pour chaque productLine
        $modelContentType = ContentTypeRegistry::get('model');
        $args['modelContentType'] = $modelContentType;

        $models = [];
        foreach ($productLines as $productLine) {
            // récupération uniquement de la liste des ids
            $qb = $repository->getFrontListQueryBuilder($request->getLocale(), $modelContentType);
            $qb
                ->select('c')
                ->innerJoin('c.contentMetas', 'cm2')
                ->andWhere('cm.format = :format and cm.metaValue LIKE :meta_value')
                ->setParameter('format', 'content_link')
                ->setParameter('meta_value', '%|'.$productLine->getId().'|%')
                ->andWhere('cm2.metaKey = :meta_key')
                ->setParameter('meta_key', 'position')
                ->orderBy('cm2.metaValue', 'ASC')
                ->setMaxResults(1)
            ;

            $model = $qb->getQuery()->getOneOrNullResult();
            $models[$productLine->getId()] = $model;
        }

        $args['models'] = $models;
    }
    public function fetchForm($data) { 
        dump('PresentationManager');dump($data);die;
        if($data['entry']->metas->form->form) {
            $id = 1;
            $form = $this->createListQueryBuilderForm($id);

            return $form;
        } else {
            return;
        }
    }
    public function createListQueryBuilderForm($id) {
        $webForm = $this->em->getRepository('InesCoreBundle:WebForm')->fetchOneActive($id);
        if (!$webForm) {
            return;
        }
        if ($webForm->getLimitEntries()) {
            $count = $this->getContentRepository()->countWebFormEntry($webForm);
            if ((int) $count >= (int) $webForm->getLimitEntries()) {
                return;
            }
        }
        $form = $this->formFactory->create(new WebFormEntryType(), null, [
            'method'   => 'POST',
            'web_form' => $webForm,
        ]);
        return [$form->createView(), $webForm];
    }

}
