<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use AppBundle\Form\Type\SectorColorType;
use Symfony\Component\Form\FormFactoryInterface;
use Ines\Bundle\CoreBundle\Form\Type\WebFormEntryType;
use Ines\Bundle\CoreBundle\Facade\ListView;


/**
 * Description of BrandManager
 *
 * @author Web
 */
class FormManager {
    protected $em;
    protected $requestStack;
    protected $formFactory;
    protected $ids;
    
    public function __construct(EntityManager $em, RequestStack $requestStack, FormFactoryInterface $formFactory) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
    }
    
    public function createListQueryBuilder() {
        $repository = $this->em->getRepository('InesCoreBundle:WebForm');
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('reference');
        
        //récupération d'un query builder
        return $repository->getFrontListQueryBuilder($locale, $contentType);
    }
    
    public function applySort(QueryBuilder $qb) {
        $qb1 = clone $qb;
        $qb1
                ->select('cm.metaValue')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'form');
        $this->ids = array_column($qb1->getQuery()->getScalarResult(), 'metaValue');
    }
    
    public function fetchAll() {
        //dump('fetchAll');
        $qb = $this->createListQueryBuilder();
        $this->applySort($qb);
        
        return $qb->getQuery()->getResult();//on déclare le service manager
    }
    
    public function fetchForm() { 
        
        if($this->ids) {
            $form = $this->createListQueryBuilderForm($this->ids[0]);
            return $form;
        } else {
            return;
        }
    }
    public function createListQueryBuilderForm($id) {
        //dump('createListQueryBuilderForm');
        $webForm = $this->em->getRepository('InesCoreBundle:WebForm')->fetchOneActive($id);
        if (!$webForm) {
            return;
        }
        if ($webForm->getLimitEntries()) {
            $count = $this->getContentRepository()->countWebFormEntry($webForm);
            if ((int) $count >= (int) $webForm->getLimitEntries()) {
                return;
            }
        }
        $form = $this->formFactory->create(new WebFormEntryType(), null, [
            'method'   => 'POST',
            'web_form' => $webForm,
        ]);
        return [$form->createView(), $webForm];
    }
}
