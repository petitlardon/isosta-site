<?php

namespace AppBundle\ContentType\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\QueryBuilder;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use AppBundle\Form\Type\SectorColorType;

/**
 * Description of PostManager
 *
 * @author Web
 */
class PostManager {
    protected $em;
    protected $requestStack;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }
    
    public function fetchList($limit) {
        $repository = $this->em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $request = $this->requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('post');
        
        //récupération d'un query builder
        //getFrontListQueryBuilder fait deja un tri sur la date de publication
        $qb = $repository->getFrontListQueryBuilder($locale, $contentType);
        
        //recuperation des 3 premiers identifiants
        $qb1 = clone $qb;
        $qb1
                ->select('c.id')->groupBy('c.id')
                ->setMaxResults($limit);
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        if(!$ids) {
            return [];
        }
        //modificationd e la requete originale pour selectionner en fonction des ids precedents
        $qb->andWhere($qb->expr()->in('c.id', $ids));
        return $qb->getQuery()->getResult();
        //on déclare le service
    }
}
