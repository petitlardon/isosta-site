<?php

namespace AppBundle\ContentType\FieldTypeExtension;

use Ines\Bundle\CoreBundle\EmbeddedForm\AbstractFieldTypeExtension;
use Ines\Bundle\CoreBundle\Entity\ContentMeta;
/**
 * Description of PageFormChoiceExtension
 *
 * @author Web
 */
class PageFormChoiceExtension extends AbstractFieldTypeExtension {
    const NAME = 'page_form_choice';
    //identique au nom qu'on lui a donné précédemment
    //refaire service tagué
    
    public function getSupports() {
        return array(
            self::SUPPORT_CONTENT,
        );
    }
    
    public function isEntity() {
        return true;
    }
//savoir comment recupérer entitée liée côté front
    public function getMappedEntities(ContentMeta $contentMeta) {
        return  [
                'source' => 'InesCoreBundle:Content',//class repo
                'id' => (int) $contentMeta->getMetaValue(),//cle primaire
                'content_meta_id' => $contentMeta->getId(),
                'multiple' => false
                ];
    }
}
