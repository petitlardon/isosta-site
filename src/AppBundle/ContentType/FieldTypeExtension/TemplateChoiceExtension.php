<?php

namespace AppBundle\ContentType\FieldTypeExtension;

use Ines\Bundle\CoreBundle\EmbeddedForm\AbstractFieldTypeExtension;
/**
 * Description of TemplateChoiceExtension
 *
 * @author Web
 */
class TemplateChoiceExtension extends AbstractFieldTypeExtension {
    const NAME = 'template_choice';
    //identique au nom qu'on lui a donné précédemment
    //refaire service tagué
}
