<?php

namespace AppBundle\ContentType\FieldTypeExtension;

use Ines\Bundle\CoreBundle\EmbeddedForm\AbstractFieldTypeExtension;
use Ines\Bundle\CoreBundle\Entity\ContentMeta;
use Ines\Bundle\CoreBundle\Entity\FieldConfig;
use Symfony\Component\Validator\Constraints\Choice;
/**
 * Description of AdvertChoiceExtension
 *
 * @author Web
 */
class ReferenceChoiceExtension extends AbstractFieldTypeExtension {
    const NAME = 'reference_choice';
    //identique au nom qu'on lui a donné précédemment
    //refaire service tagué
    
    public function getSupports() {
        return array(
            self::SUPPORT_CONTENT,
        );
    }
    
    public function isEntity() {
        return true;
    }
//savoir comment recupérer entitée liée côté front
    public function getMappedEntities(ContentMeta $contentMeta) {
        return [
                'source' => 'InesCoreBundle:Content',//class repo
                'id' => (int) $contentMeta->getMetaValue(),//cle primaire
                'content_meta_id' => $contentMeta->getId(),
                'multiple' => false
                ];
    }
    
   /* public function getFormTypeOptions(FieldConfig $config)
    {
        $options = parent::getFormTypeOptions($config);
        if ($config->getIsMultiple()) {
            $options['multiple'] = true;
        }

        $options['choices'] = $config->getChoicesAsList();
        $options['constraints'][] = new Choice(array(
            'choices'  => array_keys($options['choices']),
            'multiple' => $config->getIsMultiple(),
        ));
//dump($config);die;
        return $options;
    }*/

}
