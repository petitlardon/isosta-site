<?php

namespace AppBundle\Menu;

use Knp\Menu\Renderer\ListRenderer;
use Knp\Menu\ItemInterface;

/**
 * Description of BreadcrumbRenderer
 *
 * @author Web
 */
class BreadcrumbRenderer extends ListRenderer {
    protected function renderLinkElement(ItemInterface $item, array $options) {
            return sprintf('<a class="test" href="%s"%s>%s</a>', $this->escape($item->getUri()), $this->renderHtmlAttributes($item->getLinkAttributes()), $this->renderLabel($item, $options));            
    }
    
    protected function renderChildren(ItemInterface $item, array $options) {
        // render children with a depth - 1
        if (null !== $options['depth']) {
            $options['depth'] = $options['depth'] - 1;
        }

        if (null !== $options['matchingDepth'] && $options['matchingDepth'] > 0) {
            $options['matchingDepth'] = $options['matchingDepth'] - 1;
        }

        $html = '';
        foreach ($item->getChildren() as $child) {
            $html .= $this->renderItem($child, $options);
        }

        return $html;
    }
    
    protected function renderList(ItemInterface $item, array $attributes, array $options)
    {
        /**
         * Return an empty string if any of the following are true:
         *   a) The menu has no children eligible to be displayed
         *   b) The depth is 0
         *   c) This menu item has been explicitly set to hide its children
         */
        if (!$item->hasChildren() || 0 === $options['depth'] || !$item->getDisplayChildren()) {
            return '';
        }

        return $this->renderChildren($item, $options);
    }
    
   protected function renderItem(ItemInterface $item, array $options)
    {
        //dump($item);dump($options);die;
        // if we don't have access or this item is marked to not be shown
        if (!$item->isDisplayed()) {
            return '';
        }

        // create an array than can be imploded as a class list
        $class = (array) $item->getAttribute('class');
        $attributes = $item->getAttributes();
        if (!empty($class)) {
            $attributes['class'] = implode(' ', $class);
        }
$html = '';
        if ($this->matcher->isCurrent($item)) {
            $class[] = $options['currentClass'];
            $html .= $this->renderLink($item, $options);
        } elseif ($this->matcher->isAncestor($item, $options['matchingDepth'])) {
            $class[] = $options['ancestorClass'];
            $html .= $this->renderLink($item, $options);
            $html .= '<span class="glyphicon glyphicon-chevron-right"></span>';
        }


        // renders the embedded ul
        $childrenClass = (array) $item->getChildrenAttribute('class');

        $childrenAttributes = $item->getChildrenAttributes();
        $childrenAttributes['class'] = implode(' ', $childrenClass);

        $html .= $this->renderList($item, $childrenAttributes, $options);


        return $html;
    }
    
    protected function format($html, $type, $level, array $options)
    {
        if ($options['compressed']) {
            return $html;
        }

        switch ($type) {
            case 'ul':
            case 'link':
                $spacing = $level * 4;
                break;

            case 'li':
            case 'div':
                $spacing = $level * 4 - 2;
                break;
        }

        return str_repeat(' ', $spacing).$html."\n";
    }
}
