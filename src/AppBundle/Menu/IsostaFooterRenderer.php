<?php

namespace AppBundle\Menu;

use Knp\Menu\Renderer\ListRenderer;
use Knp\Menu\ItemInterface;

/**
 * Description of IsostaFooterRenderer
 *
 * @author Web
 */
class IsostaFooterRenderer extends ListRenderer {
    protected function renderLinkElement(ItemInterface $item, array $options) {
        if (1 == $item->getLevel()) {
            return sprintf('<span>%s</span>', $this->renderLabel($item, $options));
        } else {
            return sprintf('<a class="test" href="%s"%s>%s</a>', $this->escape($item->getUri()), $this->renderHtmlAttributes($item->getLinkAttributes()), $this->renderLabel($item, $options));            
        }
    }
}
