<?php

namespace AppBundle\Menu;

use Knp\Menu\Renderer\ListRenderer;
use Knp\Menu\ItemInterface;

/**
 * Description of IsostaRenderer
 *
 * @author Web
 */
class IsostaRenderer extends ListRenderer {
    protected function renderLinkElement(ItemInterface $item, array $options) {
        if ($item->hasChildren() and 1 == $item->getLevel()) {
            return sprintf('<a class="dropdown-toggle" data-toggle="dropdown" href="%s"%s>%s <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a>', $this->escape($item->getUri()), $this->renderHtmlAttributes($item->getLinkAttributes()), $this->renderLabel($item, $options));
        } else {
            return sprintf('<a class="test" href="%s"%s>%s</a>', $this->escape($item->getUri()), $this->renderHtmlAttributes($item->getLinkAttributes()), $this->renderLabel($item, $options));            
        }
    }
    
    protected function renderChildren(ItemInterface $item, array $options) {
        // render children with a depth - 1
        if (null !== $options['depth']) {
            $options['depth'] = $options['depth'] - 1;
        }

        if (null !== $options['matchingDepth'] && $options['matchingDepth'] > 0) {
            $options['matchingDepth'] = $options['matchingDepth'] - 1;
        }

        $html = '';
        foreach ($item->getChildren() as $child) {
            $html .= $this->renderItem($child, $options);
        }

        return $html;
    }
    
    protected function renderList(ItemInterface $item, array $attributes, array $options)
    {
        /**
         * Return an empty string if any of the following are true:
         *   a) The menu has no children eligible to be displayed
         *   b) The depth is 0
         *   c) This menu item has been explicitly set to hide its children
         */
        if (!$item->hasChildren() || 0 === $options['depth'] || !$item->getDisplayChildren()) {
            return '';
        }
        $html = '';

        $html .= $this->format('<ul'.$this->renderHtmlAttributes($attributes).'>', 'ul', $item->getLevel(), $options);
        $html .= $this->renderChildren($item, $options);
        
        $html .= $this->format('</ul>', 'ul', $item->getLevel(), $options);

        return $html;
    }
    
   protected function renderItem(ItemInterface $item, array $options)
    {
        //dump($item);dump($options);die;
        // if we don't have access or this item is marked to not be shown
        if (!$item->isDisplayed()) {
            return '';
        }

        // create an array than can be imploded as a class list
        $class = (array) $item->getAttribute('class');

         if ($this->matcher->isAncestor($item, $options['matchingDepth'])) {
            $class[] = $options['ancestorClass'];
        }elseif ($this->matcher->isCurrent($item)) {
            $class[] = $options['currentClass'];
        }

        if ($item->actsLikeFirst()) {
            $class[] = $options['firstClass'];
        }
        if ($item->actsLikeLast()) {
            $class[] = $options['lastClass'];
        }

        if ($item->hasChildren() && $options['depth'] !== 0) {
            if (null !== $options['branch_class'] && $item->getDisplayChildren()) {
                $class[] = $options['branch_class'];
            }
        } elseif (null !== $options['leaf_class']) {
            $class[] = $options['leaf_class'];
        }

        if ($item->hasChildren() and 1 == $item->getLevel() ) {
            $class[] = 'drop-down';
            $class[] = 'menu-'.strtolower($this->renderLabel($item, $options));
        }
        
        if (1 == $item->getLevel() ) {
            $class[] = 'lien-menu';
        }
        
        // retrieve the attributes and put the final class string back on it
        $attributes = $item->getAttributes();
        if (!empty($class)) {
            $attributes['class'] = implode(' ', $class);
        }

        // opening li tag
        $html = '';
         if (2 == $item->getLevel()) {
             $att['class'] = 'col-md-3 col-sm-6 col-xs-12';
             $html .= $this->format('<div'.$this->renderHtmlAttributes($att).'>', 'div', $item->getLevel(), $options);
             $html .= $this->format('<li'.$this->renderHtmlAttributes($attributes).'>', 'li', $item->getLevel(), $options);
         }
        //dump($item->getLevel());dump($options); die;
        else {
            $html .= $this->format('<li'.$this->renderHtmlAttributes($attributes).'>', 'li', $item->getLevel(), $options);
        }
        
        // render the text/link inside the li tag
        //$html .= $this->format($item->getUri() ? $item->renderLink() : $item->renderLabel(), 'link', $item->getLevel());
        $html .= $this->renderLink($item, $options);

        // renders the embedded ul
        $childrenClass = (array) $item->getChildrenAttribute('class');
        if(1 == $item->getLevel()) {
            $childrenClass[] = 'dropdown-menu mega-menu menu_level_'.$item->getLevel();
        }

        $childrenAttributes = $item->getChildrenAttributes();
        $childrenAttributes['class'] = implode(' ', $childrenClass);

        $html .= $this->renderList($item, $childrenAttributes, $options);

        // closing li tag
        if (2 == $item->getLevel()) {
             $html .= $this->format('</li>', 'li', $item->getLevel(), $options);
             $html .= $this->format('</div>', 'div', $item->getLevel(), $options);
         } else {
            $html .= $this->format('</li>', 'li', $item->getLevel(), $options);
         }


        return $html;
    }
    
    protected function format($html, $type, $level, array $options)
    {
        if ($options['compressed']) {
            return $html;
        }

        switch ($type) {
            case 'ul':
            case 'link':
                $spacing = $level * 4;
                break;

            case 'li':
            case 'div':
                $spacing = $level * 4 - 2;
                break;
        }

        return str_repeat(' ', $spacing).$html."\n";
    }
}
