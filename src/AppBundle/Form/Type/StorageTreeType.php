<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Tools\FinderService;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class StorageTreeType extends AbstractType
{
    /**
     * @var FinderService
     */
    protected $finder;

    public function __construct(FinderService $finder)
    {
        $this->finder = $finder;
    }


    public function getName()
    {
        return 'storage_tree';
    }


    public function getParent()
    {
        return 'choice';
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'required'    => false,
            'multiple'    => true,
            'expanded'    => true,
            'choice_list' => $this->finder->getStorageFolders(),
        ));
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['choices'] = $this->finder->getStorageFolders();
        $view->vars['separator'] = DIRECTORY_SEPARATOR;
    }
}
