<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use AppBundle\Form\Type\UserStorageType;

class StorageUserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('user', new UserStorageType())
            ->add('folders', 'storage_tree', array(
                'label' => 'storage.folders',
                'translation_domain' => 'InesCoreBundle',
            ))
        ;//dump($builder); die;
        //dump('StorageUserType');   die;   
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Storage',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'site_storage_user';
    }
}
