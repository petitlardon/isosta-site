<?php

namespace AppBundle\Form\Type;

use Ines\Bundle\CoreBundle\Security\Role\RoleChoice;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use \Ines\Bundle\CoreBundle\Form\Type\ProfileType;

class UserStorageType extends ProfileType
{
    protected $roleChoice;

    public function __construct()
    {
        $this->roleChoice = Array('ROLE_STORAGE');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('enabled', 'checkbox', array(
                'required' => false,
                'label'    => 'user.enabled',
            ))
            //->add('test', new StorageUserType())
            /*->add('folders', 'storage_tree', array(
                'label' => 'storage.folders',
                'translation_domain' => 'InesCoreBundle',
            ))*/
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Ines\Bundle\CoreBundle\Entity\User',
            'validation_groups' => array('Admin', 'Profile'),
            'translation_domain' => 'InesCoreBundle',
        ));
    }

    public function getName()
    {
        return 'storage_user_storage_profile';
    }
}
