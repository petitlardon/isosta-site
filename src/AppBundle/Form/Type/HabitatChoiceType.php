<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of SectorColorType
 *
 * @author Web
 */
class HabitatChoiceType extends AbstractType {   
    public static $choices = [
        "Porte d'entrée" => "porte.entree",
        "Toiture de véranda" => "toiture",
        "Volets" => "volet",
        "Panneau de remplissage" => "panneau",
    ];
        
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => static::$choices,
            'multiple'    => true,
            'expanded'    => true,
            'translation_domain' => 'messages'
        ));
    }

    public function getParent()
    {
        return 'choice';
    }
    
    public function getName() {
        return 'habitat_choice';
    }

//une fois créé, il faut le taguer pour l'appeler soctor_colopr
    
    //apres on le déclarera utilisable pour le cms => creation FieldTypeExtension
}
