<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Description of CharteColorType
 *
 * @author Web
 */
class CharteColorType extends AbstractType {
    public static $choices = [
        '#C2B504' => 'Vert',
        '#58599B' => 'Bleu',
        '#D2741D' => 'Orange',
        '#9B9C9E' => 'Gris clair',
        '#781F31' => 'Rouge',
        '#1F1E21' => 'Noir',
        '#58595B' => 'Gris foncé',
        '#FFFFFF' => 'Blanc',
        '#19345C' => 'Bleu foncé',
        '#5B589E' => 'Violet',
    ];
    
    
    //pas d'utiliosation de la class build form car c'est pas un formulaire mais un champ indépendant.
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'choices' => static::$choices,
        ]);
    }
    
    public function getParent() {
        return 'choice';
    }
    
    public function getName() {
        return 'charte_color';
    }

//une fois créé, il faut le taguer pour l'appeler soctor_colopr
    
    //apres on le déclarera utilisable pour le cms => creation FieldTypeExtension
}
