<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class StorageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', null, array(
                'label' => 'storage.user',
                'translation_domain' => 'InesCoreBundle',
                'query_builder' => function(EntityRepository $repository) {
                    return $repository->createQueryBuilder('u')
                           ->where('u.roles LIKE :role')
                           ->setParameter('role', '%ROLE_STORAGE"%');
                }
            ))
            ->add('folders', 'storage_tree', array(
                'label' => 'storage.folders',
                'translation_domain' => 'InesCoreBundle',
            ))
        ;
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Storage',
        ));
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'site_storage';
    }
}
