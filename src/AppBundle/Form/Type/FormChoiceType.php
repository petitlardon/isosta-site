<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Description of AdvertChoiceType
 *
 * @author Web
 */
class FormChoiceType extends AbstractType {
    protected $repository;
    protected $locale;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->repository = $em->getRepository('InesCoreBundle:WebForm');
        
        $request = $requestStack->getMasterRequest();
        $this->locale = $request->getLocale();
    }
    
    //pas d'utiliosation de la class build form car c'est pas un formulaire mais un champ indépendant.
    public function configureOptions(OptionsResolver $resolver) {
        // requête pour récupérer tous les sliders
        $qb = $this->repository->createQueryBuilder('f');
        $qb
                ->select('f.id, f.name')
                ->orderBy('f.name', 'ASC')
                ;
        $result = $qb->getQuery()->getScalarResult();
        
        $choices = array_column($result, 'name', 'id');
        //tableau aura id + label des sliders disponibles
        $resolver->setDefaults([
            'choices' => $choices,
        ]);//on enregistre le service
    }
    
    public function getParent() {
        return 'choice';
    }
    
    public function getName() {
        return 'form_choice';
    }

}
