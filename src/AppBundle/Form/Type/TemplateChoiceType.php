<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Description of SectorColorType
 *
 * @author Web
 */
class TemplateChoiceType extends AbstractType {
    public static $choices = [
        'layout_1' => 'Gabarit sommaire produit',
        'layout_2' => 'Gabarit sommaire produit encadré',
        'layout_3' => 'Gabarit niveau 2 simple',
        'page_produit' => 'Gabarit page produit',
        'layout_4' => 'Gabarit niveau 2 complet',
        'catalogue_produit' => 'Gabarit catalogue porte',
        'fiche_produit' => 'Gabarit fiche chantier',
        'group' => 'Gabarit page groupe',
        'form' => 'Affichage de formulaire',
    ];
    
    
    //pas d'utiliosation de la class build form car c'est pas un formulaire mais un champ indépendant.
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'choices' => static::$choices,
        ]);
    }
    
    public function getParent() {
        return 'choice';
    }
    
    public function getName() {
        return 'template_choice';
    }

//une fois créé, il faut le taguer pour l'appeler soctor_colopr
    
    //apres on le déclarera utilisable pour le cms => creation FieldTypeExtension
}
