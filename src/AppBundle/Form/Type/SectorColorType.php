<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Description of SectorColorType
 *
 * @author Web
 */
class SectorColorType extends AbstractType {
    public static $choices = [
        '#c2b506' => 'Habitat',
        '#5b589f' => 'Bâtiment',
        '#d4711c' => 'Industrie',
    ];
    
    
    //pas d'utiliosation de la class build form car c'est pas un formulaire mais un champ indépendant.
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'choices' => static::$choices,
        ]);
    }
    
    public function getParent() {
        return 'choice';
    }
    
    public function getName() {
        return 'sector_color';
    }

//une fois créé, il faut le taguer pour l'appeler soctor_colopr
    
    //apres on le déclarera utilisable pour le cms => creation FieldTypeExtension
}
