<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Description of SectorColorType
 *
 * @author Web
 */
class SliderChoiceType extends AbstractType {
    protected $repository;
    protected $locale;
    
    public function __construct(EntityManager $em, RequestStack $requestStack) {
        $this->repository = $em->getRepository('InesCoreBundle:Content');
        
        $request = $requestStack->getMasterRequest();
        $this->locale = $request->getLocale();
    }
    
    //pas d'utiliosation de la class build form car c'est pas un formulaire mais un champ indépendant.
    public function configureOptions(OptionsResolver $resolver) {
        // requête pour récupérer tous les sliders
        $qb = $this->repository->createQueryBuilder('c');
        $qb
                ->select('c.id, c.title')
                ->where('c.contentType = :ct')
                ->setParameter('ct', 'slider')
                ->andWhere('c.language = :lang')
                ->setParameter('lang', $this->locale)
                ->orderBy('c.title', 'ASC')
                ;
        $result = $qb->getQuery()->getScalarResult();
        
        $choices = array_column($result, 'title', 'id');
        //tableau aura id + label des sliders disponibles
        $resolver->setDefaults([
            'choices' => $choices,
        ]);//on enregistre le service
    }
    
    public function getParent() {
        return 'choice';
    }
    
    public function getName() {
        return 'slider_choice';
    }

}
