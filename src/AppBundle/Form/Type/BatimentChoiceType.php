<?php

namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of SectorColorType
 *
 * @author Web
 */
class BatimentChoiceType extends AbstractType {   
    public static $choices = [
        "EDR" => "edr",
        "Façade" => "facade",
        "ITE" => "ite",
    ];
        
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => static::$choices,
            'multiple'    => true,
            'expanded'    => true,
            'translation_domain' => 'messages'
        ));
    }

    public function getParent()
    {
        return 'choice';
    }
    
    public function getName() {
        return 'batiment_choice';
    }

//une fois créé, il faut le taguer pour l'appeler soctor_colopr
    
    //apres on le déclarera utilisable pour le cms => creation FieldTypeExtension
}
