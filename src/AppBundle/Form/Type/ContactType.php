<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('main_address_name', null, ['label' => 'contact.address_name'])
            ->add('main_address', null, ['label' => 'contact.address'])
            ->add('main_phone', null, ['label' => 'contact.phone'])
            ->add('main_fax', null, ['label' => 'contact.fax'])
            ->add('main_email', 'email', [
                'required' => false,
                'label' => 'contact.email'
            ])
            ->add('first_service_name', null, ['label' => 'contact.service_name'])
            ->add('first_service_phone', null, ['label' => 'contact.phone'])
            ->add('first_service_resp', null, ['label' => 'contact.service_resp'])
            ->add('second_service_name', null, ['label' => 'contact.service_name'])
            ->add('second_service_phone', null, ['label' => 'contact.phone'])
            ->add('second_service_resp', null, ['label' => 'contact.service_resp'])
            ->add('third_service_name', null, ['label' => 'contact.service_name'])
            ->add('third_service_phone', null, ['label' => 'contact.phone'])
            ->add('third_service_resp', null, ['label' => 'contact.service_resp'])
            ->add('fourth_service_name', null, ['label' => 'contact.service_name'])
            ->add('fourth_service_phone', null, ['label' => 'contact.phone'])
            ->add('fourth_service_resp', null, ['label' => 'contact.service_resp'])
            ->add('facebook', 'url', [
                'required' => false,
                'label' => 'contact.facebook'
            ])
            ->add('twitter', 'url', [
                'required' => false,
                'label' => 'contact.twitter'
            ])
            ->add('google', 'url', [
                'required' => false,
                'label' => 'contact.google'
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'AppBundle\Entity\Contact',
            'translation_domain' => 'InesCoreBundle',
            'method'             => 'PUT'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'site_sitebundle_contact';
    }
}
