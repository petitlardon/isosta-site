<?php

namespace AppBundle\Controller;

use Ines\Bundle\CoreBundle\Controller\BaseController;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Contact;
use AppBundle\Form\Type\ContactType;

/**
 * Contact controller.
 */
class ContactController extends BaseController
{

    /**
     * @Route("/", name="site_admin_contact")
     * @Method({"GET", "PUT"})
     */
    public function indexAction(Request $request)
    {
        // Entity
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Contact')->findAll();
        $entity = empty($entities) ? new Contact() : $entities[0] ;

        // Form
        $form = $this->createForm(new ContactType(),$entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->addFlash('success','contact.saved');
        }

        return $this->render('AppBundle:Contact:index.html.twig', [
            'form'   => $form->createView(),
            'entity' => $entity,
        ]);
    }
}
