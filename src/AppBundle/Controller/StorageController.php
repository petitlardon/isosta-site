<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use Ines\Bundle\CoreBundle\Facade\ListView;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Storage controller.
 *
 * @Route("/storage")
 */
class StorageController extends Controller
{

    /**
     * Lists all Storage entities.
     *
     * @Route("/", name="site_storage", options={"expose" = true})
     * @Method("GET")
     * 
     * @Security("has_role('ROLE_STORAGE')")
     */
    public function indexAction()
    {
        $isManager = $this->get('security.context')->isGranted('ROLE_STORAGE_MANAGER');
        
        $request = $this->get('request');
        $pathes = array($request->getLocale());
        if (!$isManager) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Storage')
                         ->findOneByUser($this->getUser());
            $pathes = $entity->getFolders();
        }
        
        $this->get('site.finder')->getStorageTree($pathes);

        return $this->render('AppBundle:Storage:index.html.twig', array(
            'folders' => $this->get('site.finder')->getStorageTree($pathes)
        ));
    }
    
    /**
     * Lists all Storage entities.
     *
     * @Route("/public", name="site_documentation", options={"expose" = true})
     * @Method("GET")
     */
    public function publicAction()
    {
        $request = $this->get('request');
        $pathes = array($request->getLocale());
        return $this->render('AppBundle:Storage:public.html.twig', array(
            'folders' => $this->get('site.finder')->getStorageTree($pathes),
            'template' => ''
        ));
    }

    /**
     * Lists all Storage entities.
     *
     * @Route("/tim_doc", name="tim_documentation", options={"expose" = true})
     * @Method("GET")
     */
    public function timAction()
    {
        $request = $this->get('request');
        $pathes = array($request->getLocale());
        //$pathes = array('public/batiment/TIM Composites');
        return $this->render('AppBundle:Storage:public.html.twig', array(
            'folders' => $this->get('site.finder')->getStorageTree($pathes),
            'template' => 'TIM'
        ));
    }

        /**
     * @Route(path="/login", name="storage_login")
     * @Method({"GET", "POST"})
     */
    public function loginAction() {
        $request = $this->container->get('request');
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session\Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');
        $template = 'AppBundle:Storage:login.html.twig';
        if($request->attributes->has('light') && true == $request->attributes->get('light')) {
            $template = 'AppBundle:Storage:login_light.html.twig';
        }
        return $this->render($template, array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token' => $csrfToken,
        ));
    }
    
    /**
     * @Route(path="/check", name="storage_check")
     */
    public function checkAction() {
        
    }
    
    /**
     * @Route(path="/logout", name="storage_logout")
     */
    public function logoutAction() {

    }



    /**
     * Download file.
     *
     * @Route("/{path}", name="site_storage_file")
     * @Method("GET")
     */
    public function downloadAction($path)
    {
        $context = $this->get('security.context');
       /** if (false === $context->isGranted('ROLE_STORAGE')) {
            $message = $this->get('translator')
                            ->trans('storage.not_allowed');
            throw $this->createAccessDeniedException($message);
        }*/

        $path = urldecode($path);
        
        $absolutePath = $this->container->getParameter('site.storage.root')
                      . DIRECTORY_SEPARATOR . $path;
        $request = $this->get('request');
        $folders = array($request->getLocale());
        
        if (!is_file($absolutePath)) {
            $message = $this->get('translator')
                            ->trans('storage.file.not_found');
            throw $this->createNotFoundException($message);
        }

        if (true === $context->isGranted('ROLE_ADMIN')) {
            return $this->getFileResponse($absolutePath);
        }
        if (true === $context->isGranted('ROLE_STORAGE')) {
            $folders[] = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('AppBundle:Storage')
                    ->findOneByUser($this->getUser())
                    ->getFolders();
        } else {
            $folders[] = 'public';
        }
        $success = false;

        foreach ($folders as $folder) {
            if (strpos($path, $folder . '/') === 0) {
                $success = true;
                break;
            }
        }

        if (!$success) {
            $message = $this->get('translator')
                            ->trans('storage.file.not_allowed');
            throw $this->createAccessDeniedException($message);
        }

        return $this->getFileResponse($absolutePath);
   }

   protected function getFileResponse($path)
   {
        $parts = explode(DIRECTORY_SEPARATOR, $path);

        $response = new BinaryFileResponse($path);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            array_pop($parts)
        );

        return $response;
   }
   
    public function sliderAction() {
        $results = $this->get('manager.slider')->fetchOneByMetakey('is_storage');
        $contents = ListView::buildView($results);
       
        return $this->render('AppBundle:Component:slider.html.twig', [
            'contents' => $contents,
        ]);
    }

    public function advertAction() {
        $results = $this->get('manager.advert')->fetchOneByMetakey('is_storage');
        $contents = ListView::buildView($results);
       
        return $this->render('AppBundle:Component:advert.html.twig', [
            'contents' => $contents,
        ]);
    }

}
