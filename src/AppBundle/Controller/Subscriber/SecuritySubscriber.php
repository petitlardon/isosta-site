<?php

namespace AppBundle\Controller\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\ThemeBundle\ThemeEvents;
use Ines\Bundle\ThemeBundle\Event\ContentQueryEvent;
use AppBundle\ContentType\Manager\BrandManager;
use Ines\Bundle\ThemeBundle\Event\ContentRenderEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Description of BrandContentSubscriber
 *
 * @author Web
 */
class SecuritySubscriber implements EventSubscriberInterface {
    
    protected $authChecker;
    
    public function __construct(AuthorizationCheckerInterface $authChecker) {
        $this->authChecker = $authChecker;
    }
    
    public static function getSubscribedEvents() {
        //on s'accroche au pre_query_list. Le dispatcher lancera toutes les méthodes accrochées au pre_query_list. donc celle là en plus
        return [
            ThemeEvents::PRE_RENDER_SINGLE => 'onPreRenderSingle',
            // pre_render car $this->get('event_dispatcher')->dispatch(ThemeEvents::PRE_RENDER_LIST, new ContentRenderEvent($contentType, $viewArg)); dans controller ines theme
        ];
    }
    
    public function onPreRenderSingle(ContentRenderEvent $event) {
        $contentType = $event->getContentType();
        $args = $event->getViewArgument();
        $entry = $args['entry'];
        
        if(!isset($entry->metas->security)) {
            return;
        }
        
        if(true === @$entry->metas->security->is_private && !$this->authChecker->isGranted('ROLE_ISOSTA')) {
            throw new AccessDeniedException('Private page');
        }
    }
}
