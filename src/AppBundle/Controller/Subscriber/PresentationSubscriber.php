<?php

namespace AppBundle\Controller\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\ThemeBundle\ThemeEvents;
use Ines\Bundle\ThemeBundle\Event\ContentQueryEvent;
use AppBundle\ContentType\Manager\PresentationManager;
use Ines\Bundle\ThemeBundle\Event\ContentRenderEvent;

/**
 * Description of BrandContentSubscriber
 *
 * @author Web
 */
class PresentationSubscriber implements EventSubscriberInterface {
    
    protected $manager;
    
    public function __construct(PresentationManager $manager) {
        $this->manager = $manager;
    }
    
    public static function getSubscribedEvents() {
        //on s'accroche au pre_query_list. Le dispatcher lancera toutes les méthodes accrochées au pre_query_list. donc celle là en plus
        return [
            ThemeEvents::PRE_QUERY_SINGLE => 'onPreQuerySingle',
            ThemeEvents::PRE_RENDER_SINGLE => 'onPreRenderSingle',
            // pre_render car $this->get('event_dispatcher')->dispatch(ThemeEvents::PRE_RENDER_LIST, new ContentRenderEvent($contentType, $viewArg)); dans controller ines theme
        ];
    }
    
    public function onPreQuerySingle(ContentQueryEvent $event) {
        $contentType = $event->getContentType();
        $qb = $event->getQueryBuilder();
        
        if('presentation' !== $contentType->getSlug()) {
            return;
        }
       //$test = $this->manager->applySort($qb);
        //dump($qb);dump($test);die;
    }

    public function onPreRenderSingle(ContentRenderEvent $event) {
        $contentType = $event->getContentType();

        if('presentation' !== $contentType->getSlug()) {
            return;
        }
        $args = $event->getViewArgument();
        $reference = $args['entry']->metas->references;

        //$this->manager->fetchReferences($reference, $args);
        
        //$args['toto'] = 'tata';
        //dump($args);die;
        //pas de return car passé en référence
    }

//put your code here
}
