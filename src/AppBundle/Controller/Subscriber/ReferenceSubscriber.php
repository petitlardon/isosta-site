<?php

namespace AppBundle\Controller\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Ines\Bundle\ThemeBundle\ThemeEvents;
use Ines\Bundle\ThemeBundle\Event\ContentQueryEvent;
use AppBundle\ContentType\Manager\ReferenceManager;
use Ines\Bundle\ThemeBundle\Event\ContentRenderEvent;

/**
 * Description of BrandContentSubscriber
 *
 * @author Web
 */
class ReferenceSubscriber implements EventSubscriberInterface {
    
    protected $manager;
    
    public function __construct(ReferenceManager $manager) {
        $this->manager = $manager;
    }
    
    public static function getSubscribedEvents() {
        //on s'accroche au pre_query_list. Le dispatcher lancera toutes les méthodes accrochées au pre_query_list. donc celle là en plus
        return [
            ThemeEvents::PRE_QUERY_SINGLE => 'onPreQuerySingle',
            ThemeEvents::PRE_RENDER_SINGLE => 'onPreRenderSingle',
            // pre_render car $this->get('event_dispatcher')->dispatch(ThemeEvents::PRE_RENDER_LIST, new ContentRenderEvent($contentType, $viewArg)); dans controller ines theme
        ];
    }
    
    public function onPreQuerySingle(ContentQueryEvent $event) {
        $contentType = $event->getContentType();
        $qb = $event->getQueryBuilder();
        
        if('reference' !== $contentType->getSlug()) {
            return;
        }
        $this->manager->applySort($qb);
        
    }

    public function onPreRenderSingle(ContentRenderEvent $event) {
        $contentType = $event->getContentType();

        if('reference' !== $contentType->getSlug()) {
            return;
        }
        $args = $event->getViewArgument();
        $args['toto'] = 'tata';
        dump($args);die;
        //pas de return car passé en référence
    }

//put your code here
}
