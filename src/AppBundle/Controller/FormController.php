<?php

namespace AppBundle\Controller;

use Ines\Bundle\CoreBundle\EmbeddedForm\WebFormEntryEvent;
use Ines\Bundle\CoreBundle\EmbeddedForm\WebFormEntryEvents;
use Ines\Bundle\CoreBundle\Facade\WebFormEntryManager;
use Ines\Bundle\CoreBundle\Form\Type\WebFormEntryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FormController extends Controller
{
    public function createAction($webFormId, Request $request)
    {
        
        $webForm = $this->getWebFormRepository()->fetchOneActive($webFormId);
        if (!$webForm) {
            throw $this->createNotFoundException();
        }
        if ($webForm->getLimitEntries()) {
            // see if entry count is less then specified limit
            $count = $this->getContentRepository()->countWebFormEntry($webForm);
            if ((int) $count >= (int) $webForm->getLimitEntries()) {
                throw $this->createNotFoundException();
            }
        }

        $form = $this->createForm(new WebFormEntryType(), null, [
            'method'   => 'POST',
            'web_form' => $webForm,
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entry = WebFormEntryManager::create($webForm, $request->getLocale(), $form->getData());

            $em = $this->getEntityManager();
            $em->persist($entry);
            $em->flush();

            $this->get('event_dispatcher')->dispatch(WebFormEntryEvents::CREATE, new WebFormEntryEvent($webForm, $entry));

            $this->get('session')->getFlashBag()->add('success', 'webform.flash.front.submit_success');

            return $this->redirect($this->getRedirectUrl($request));
        }

        $this->get('session')->getFlashBag()->add('error', 'webform.flash.front.submit_error');

        return $this->redirect($this->getRedirectUrl($request));
    }

    protected function getRedirectUrl(Request $request)
    {
        
        //$url = $request->headers->get('referer');

            if ($request->getLocale() != 'fr') {
                $url = $this->generateUrl('ines_theme_homepage_i18n', ['_locale' => $request->getLocale()]);
            } else {
                $url = $this->generateUrl('site_formulaire_valide');
            }
        return $url;
    }

    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    protected function getContentRepository()
    {
        return $this->getEntityManager()
            ->getRepository('InesCoreBundle:Content');
    }

    protected function getWebFormRepository()
    {
        return $this->getEntityManager()
            ->getRepository('InesCoreBundle:WebForm');
    }
}
