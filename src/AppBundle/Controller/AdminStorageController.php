<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Storage;
use AppBundle\Entity\StorageRepository;
use Symfony\Component\Form\FormError;
use Doctrine\DBAL\DBALException;
use Ines\Bundle\CoreBundle\Entity\User;
// TODO Refactoriser

/**
 * Storage controller.
 *
 * @Route("/")
 */
class AdminStorageController extends Controller
{
    /**
     * @var StorageRepository
     */
    protected $repository;

    /**
     * Lists all Storage entities.
     *
     * @Route("/", name="site_admin_storage")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Storage')->findAll();
        $deleteForms = array();

        foreach ($entities as $entity) {
            $deleteForms[] = $this->createDeleteForm($entity->getId())->createView();
        }

        return $this->render('AppBundle:Storage:admin/index.html.twig', array(
            'entities' => $entities,
            'delete_forms' => $deleteForms
        ));
    }


    /**
     * Creates a new Storage entity.
     *
     * @Route("/", name="site_admin_storage_create")
     * @Method("POST")
     * @Template("AppBundle:Storage:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Storage();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            try {
                $em->flush();
                return $this->redirect($this->generateUrl('site_admin_storage_edit', array('id' => $entity->getId())));
            } catch (DBALException $ex) {
                $form->get('user')->addError(new FormError('Cet utilisateur est déjà lié à l\'espace documentaire.'));
            }
        }

        return $this->render('AppBundle:Storage:admin/view.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Storage entity.
     *
     * @param Storage $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Storage $entity)
    {
        $this->repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Storage');

        $form = $this->createForm('site_storage', $entity, array(
            'action' => $this->generateUrl('site_admin_storage_create'),
            'method' => 'POST',
        ));
        
        $form->add('submit', 'submit', array(
            'label' => 'create',
            'attr' => array(
                'class' => 'btn-primary'
            )
        ));
        
        return $form;
    }


    /**
     * Displays a form to create a new Storage entity.
     *
     * @Route("/new", name="site_admin_storage_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Storage();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Storage:admin/view.html.twig', array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        ));
    }


    
    /**
     * Enregistrement d'un nouveau membre
     *
     * @Route("/new_user", name="site_admin_storage_new_user")
     * @Method("GET|POST")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $storage = new Storage();
        $form   = $this->createCreateFormUser($storage);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $user = $storage->getUser();
            $user->addRole('ROLE_STORAGE'); 
            $user->addRole('ROLE_ISOSTA'); 
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($storage);
            try {
                $em->flush();
                return $this->redirect($this->generateUrl('site_admin_storage_edit', array('id' => $storage->getId())));
            } catch (DBALException $ex) {
                $form->get('user')->addError(new FormError('Cet utilisateur est déjà lié à l\'espace documentaire.'));
            }
        }

        return $this->render('AppBundle:Storage:admin/view.html.twig', array(
            'entity'      => $storage,
            'form'   => $form->createView(),
        ));
    }


    /**
     * Creates a form to create a Storage entity.
     *
     * @param Storage $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateFormUser(Storage $storage)
    {
        $this->repository = $this->getDoctrine()->getManager()->getRepository('InesCoreBundle:User');
        $form = $this->createForm('site_storage_user', $storage, array(
            'action' => $this->generateUrl('site_admin_storage_new_user'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array(
            'label' => 'create',
            'attr' => array(
                'class' => 'btn-primary'
            )
        ));
        return $form;
    }
    private function createUser(User $user)
    {
        $this->repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Storage');
        
        $form = $this->createForm('storage_user_profile', null, array(
            'action' => $this->generateUrl('site_admin_storage_create_user'),
            'method' => 'POST',
        ));
        return $form;
    }

    /**
     * Displays a form to edit an existing Storage entity.
     *
     * @Route("/{id}", name="site_admin_storage_edit")
     * @Method("GET")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Storage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Storage entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Storage:admin/view.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
    * Creates a form to edit a Storage entity.
    *
    * @param Storage $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Storage $entity)
    {
        $form = $this->createForm('site_storage_user', $entity, array(
            'action' => $this->generateUrl('site_admin_storage_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'update',
                'attr' => [
                    'class' => 'btn-primary'
                ]
            ));

        return $form;
    }


    /**
     * Edits an existing Storage entity.
     *
     * @Route("/{id}", name="site_admin_storage_update")
     * @Method("PUT")
     * @Template("AppBundle:Storage:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Storage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Storage entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('site_admin_storage_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Deletes a Storage entity.
     *
     * @Route("/{id}", name="site_admin_storage_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Storage')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Storage entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('site_admin_storage'));
    }


    /**
     * Creates a form to delete a Storage entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('site_admin_storage_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Delete',
                'attr' => [
                    'class' => 'btn-danger'
                ]
            ))
            ->getForm()
        ;
    }
    
    

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        $email = $this->container->get('session')->get('fos_user_send_confirmation_email/email');
        $this->container->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:checkEmail.html.'.$this->getEngine(), array(
            'user' => $user,
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction($token)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());
        $user->addRole(array('ROLE_STORAGE'));

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $response = new RedirectResponse($this->container->get('router')->generate('fos_user_registration_confirmed'));
        $this->authenticateUser($user, $response);

        return $response;
    }

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:confirmed.html.'.$this->getEngine(), array(
            'user' => $user,
        ));
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }

    protected function getEngine()
    {
        return $this->container->getParameter('fos_user.template.engine');
    }

}
