<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ines\Bundle\CoreBundle\Facade\ContentTypeManager;
use Ines\Bundle\CoreBundle\Facade\ListView;
use AppBundle\Form\Type\SectorColorType;

/**
 * Description of ComponentController
 *
 * @author Web
 */
class ComponentController extends Controller {
    public function brandAction(){
        /*$em = $this->getDoctrine()->getManager();
        //recup repo pour les contenus
        $repository = $em->getRepository('InesCoreBundle:Content');
        
        //récupération de  la langue courante
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getMasterRequest();
        $locale = $request->getLocale();
        
        //récupération du contentType
        $contentType = ContentTypeManager::fetchOne('brand');
        
        //récupération d'un query builder
        $qb = $repository->getFrontListQueryBuilder($locale, $contentType);
        
        
        //récupération des valeurs de couleur dans l'ordre
        //comme c du string on protege le string
        //$colors = array_keys(SectorColorType::$choices);
        
        //arraymap
        $colors = array_map(function($color) use ($qb) {
            return $qb->expr()->literal($color);
        }, array_keys(SectorColorType::$choices));
        $qb1 = clone $qb;
        $qb1
                ->select('c.id, FIELD(cm.metaValue, '. implode(',', $colors).') AS HIDDEN field')->groupBy('c.id')
                ->andWhere('cm.metaKey = :key')
                ->setParameter('key', 'color')
                ->orderBy('field');
        $ids = array_column($qb1->getQuery()->getScalarResult(), 'id');
        if($ids) {
            $qb
                    ->addSelect('FIELD(c.id, '. implode(',', $ids).') AS HIDDEN field')
                    ->orderBy('field');
        }
        
        //récupération des résultats
        $results = $qb->getQuery()->getResult();*/
        
        $results = $this->get('manager.brand')->fetchAll();
        
        
        //chaque result est un contenu (table ines_content
        $contents = ListView::buildView($results);
        //on va faire un ùmanager pour alléger et réutilkiser le code
        
        return $this->render('AppBundle:Component:brand.html.twig', [
            'contents' => $contents
        ]);
    }
    
    public function postAction() {
        $results = $this->get('manager.post')->fetchList(3);
        $contents = ListView::buildView($results);

        return $this->render('AppBundle:Component:post.html.twig', [
            'contents' => $contents,
            // ContentTypeManager facade pour éviter un appel du genre $this->get('manager.post')
            'contentType' => ContentTypeManager::fetchOne('post')
        ]);
    }

    public function sectorAction() {
        $results = $this->get('manager.sector')->fetchAll();
        $contents = ListView::buildView($results);

        return $this->render('AppBundle:Component:sector.html.twig', [
            'contents' => $contents,
        ]);
    }

    public function sliderAction() {
        $results = $this->get('manager.slider')->fetchOneByMetakey('is_home');
        $contents = ListView::buildView($results);
       
        return $this->render('AppBundle:Component:slider.html.twig', [
            'contents' => $contents,
        ]);
    }

    public function contactAction() {
        $results = $this->get('manager.contact')->fetchAll();
        $contents = ListView::b00uildView($results);
       
        return $this->render('AppBundle:Component:slider.html.twig', [
            'contents' => $contents,
        ]);
    }

    public function timelineAction() {
        $results = $this->get('manager.timeline')->fetchAll();
        $contents = ListView::buildView($results);
       
        return $this->render('AppBundle:theme:template/_timeline.html.twig', [
            'contents' => $contents,
        ]);
    }

    }
