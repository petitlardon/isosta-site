<?php

namespace AppBundle\Twig;

use Doctrine\ORM\EntityManager;

class AppExtension extends \Twig_Extension
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            'get_the_media' => new \Twig_Function_Method($this, 'getTheMedia'),
        ];
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter(
                'movie',
                [$this, 'movieFilter'],
                ['is_safe' => ['html']]
            )
        ];

    }

    public function movieFilter($youtubeId, array $options = array())
    {
        $options = array_replace(array(
            'class'  => 'embed-responsive-item',
            'width'  => 960,
            'height' => 720
        ), $options);

        $url = sprintf('https://www.youtube.com/embed/%s?rel=0', $youtubeId);

        $template = '<iframe class="%s" src="%s" width="%d" height="%d" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" allowfullscreen ></iframe>';

        return vsprintf($template, [
            $options['class'],
            $url,
            $options['width'],
            $options['height']
        ]);
    }

    public function getTheMedia($id, array $medias = [])
    {
        if ($medias) {
            $filteredMedias = array_filter($medias, function($media) use ($id) {
                return $id == $media->getId();
            });

            if ($filteredMedias) {
                return current($filteredMedias);
            }
        }

        return $this->em->getRepository('InesCoreBundle:Media')->find($id);
    }

    public function getName()
    {
        return 'app';
    }
}