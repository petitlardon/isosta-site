<?php

namespace AppBundle\Tools;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Finder\SplFileInfo;

class FinderService
{
    protected $storageRoot;
    protected $storageFinder;

    public function __construct($storageRoot)
    {
        $this->storageRoot = $storageRoot;
    }

    /**
     * @param array|null $pathes
     * @return Finder
     */
    public function getStorageFileFinder($pathes = null)
    {
        if ($pathes === null) {
            $pathes = array();
        }

        $finder = new Finder();
        $finder->in($this->storageRoot)->sortByName();

        foreach ($pathes as $path) {
            $finder->path($path . '/');
        }
        

        return $finder;
    }


    /**
     * @param array $pathes
     * @return array
     */
    public function getStorageTree($pathes = null)
    {
        $finder = $this->getStorageFileFinder($pathes);

        $tree = array();

        foreach ($finder as $file) {

            $pathArray = $file->isDir() ?
                         array($file->getFilename() => array()) :
                         ($file->isFile() ? array($file) : null);

            if ($pathArray === null) {
                continue;
            }

            $parts = array_filter(explode(DIRECTORY_SEPARATOR, $file->getRelativePath()));

            if (!empty($parts)) {
                while($key = array_pop($parts)) {
                    $pathArray = array($key => $pathArray);
                }
                $tree = $this->array_merge_recursive_distinct($tree, $pathArray);
                //$tree = array_merge_recursive($tree, $pathArray);
            }

        }
        $this->rksort($tree);

        return $tree;
    }
    
    public function array_merge_recursive_distinct ($arr,$ins) {
        if(is_array($arr))
        {
            if(is_array($ins)) foreach($ins as $k=>$v)
            {
                if(isset($arr[$k])&&is_array($v)&&is_array($arr[$k]))
                {
                    $arr[$k] = $this->array_merge_recursive_distinct($arr[$k],$v);
                }
                else {
                    // This is the new loop :)
                    while (isset($arr[$k]))
                        $k++;
                    $arr[$k] = $v;
                }
            }
        }
        elseif(!is_array($arr)&&(strlen($arr)==0||$arr==0))
        {
            $arr=$ins;
        }
        return($arr);
      /*$merged = $array1;

      foreach ( $array2 as $key => &$value )
      {
        if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
        {
          $merged [$key] = $this->array_merge_recursive_distinct ( $merged [$key], $value );
        }
        else
        {
          $merged [$key] = $value;
        }
      }

      return $merged;*/
        
    }


    /**
     * @var ChoiceList
     */
    protected $storageRootFolders;


    /**
     * @param bool $refresh
     * @return ChoiceList
     */
    public function getStorageRootFolders($refresh = false)
    {
        if ($this->storageRootFolders === null || $refresh) {

            $this->storageRootFolders = $this->finderToChoiceList(
                $this->getStorageFolderFinder()->depth(0)
            );
        }

        return $this->storageRootFolders;
    }


    /**
     * @var ChoiceList
     */
    protected $storageFolders;


    /**
     * @param bool $refresh
     * @return ChoiceList
     */
    public function getStorageFolders($refresh = false)
    {
        if ($this->storageFolders === null || $refresh) {

            $this->storageFolders = $this->finderToChoiceList(
                $this->getStorageFolderFinder()->depth(2)
            );
        }

        return $this->storageFolders;
    }



    // TOOLS


    /**
     * @return Finder
     */
    protected function getStorageFolderFinder()
    {
        $finder = new Finder();
        return $finder->in($this->storageRoot)->directories()->sortByName();
    }


    /**
     * @param Finder $finder
     * @return ChoiceList
     */
    protected function finderToChoiceList(Finder $finder)
    {
        $choices = array_map(function(SplFileInfo $value) {
            return $value->getRelativePathname();
        }, iterator_to_array($finder));

        $labels = array_map(function(SplFileInfo $value) {
            return $value->getFilename();
        }, iterator_to_array($finder));

        return new ChoiceList($choices, $labels);
    }


    protected function rksort(&$array) {

        ksort($array);

        foreach ($array as &$item) {
            if (is_array($item)){
                $this->rksort($item);
            }
        }

    }

}
