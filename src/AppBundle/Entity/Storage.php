<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Storage
 *
 * @ORM\Table(name="site_storage")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\StorageRepository")
 */
class Storage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Ines\Bundle\CoreBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var array
     *
     * @ORM\Column(name="folders", type="simple_array")
     */
    private $folders;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userID
     *
     * @param integer $user
     * @return Storage
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userID
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set folders
     *
     * @param array $folders
     * @return Storage
     */
    public function setFolders($folders)
    {
        $this->folders = $folders;

        return $this;
    }

    /**
     * Get folders
     *
     * @return array
     */
    public function getFolders()
    {
        return $this->folders;
    }
}
