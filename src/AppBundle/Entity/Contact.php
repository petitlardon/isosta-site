<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ContactRepository")
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="main_address_name", type="string", length=255, nullable=true)
     */
    private $main_address_name;

    /**
     * @var string
     *
     * @ORM\Column(name="main_address", type="text", nullable=true)
     */
    private $main_address;

    /**
     * @var string
     *
     * @ORM\Column(name="main_phone", type="string", length=255, nullable=true)
     */
    private $main_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="main_fax", type="string", length=255, nullable=true)
     */
    private $main_fax;

    /**
     * @var string
     *
     * @ORM\Column(name="main_email", type="string", length=255, nullable=true)
     */
    private $main_email;

    /**
     * @var string
     *
     * @ORM\Column(name="first_service_name", type="string", length=255, nullable=true)
     */
    private $first_service_name;

    /**
     * @var string
     *
     * @ORM\Column(name="first_service_phone", type="string", length=255, nullable=true)
     */
    private $first_service_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="first_service_resp", type="string", length=255, nullable=true)
     */
    private $first_service_resp;

    /**
     * @var string
     *
     * @ORM\Column(name="second_service_name", type="string", length=255, nullable=true)
     */
    private $second_service_name;

    /**
     * @var string
     *
     * @ORM\Column(name="second_service_phone", type="string", length=255, nullable=true)
     */
    private $second_service_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="second_service_resp", type="string", length=255, nullable=true)
     */
    private $second_service_resp;

    /**
     * @var string
     *
     * @ORM\Column(name="third_service_name", type="string", length=255, nullable=true)
     */
    private $third_service_name;

    /**
     * @var string
     *
     * @ORM\Column(name="third_service_phone", type="string", length=255, nullable=true)
     */
    private $third_service_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="third_service_resp", type="string", length=255, nullable=true)
     */
    private $third_service_resp;

    /**
     * @var string
     *
     * @ORM\Column(name="fourth_service_name", type="string", length=255, nullable=true)
     */
    private $fourth_service_name;

    /**
     * @var string
     *
     * @ORM\Column(name="fourth_service_phone", type="string", length=255, nullable=true)
     */
    private $fourth_service_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fourth_service_resp", type="string", length=255, nullable=true)
     */
    private $fourth_service_resp;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="google", type="string", length=255, nullable=true)
     */
    private $google;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Social
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Social
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set google
     *
     * @param string $google
     * @return Social
     */
    public function setGoogle($google)
    {
        $this->google = $google;

        return $this;
    }

    /**
     * Get google
     *
     * @return string
     */
    public function getGoogle()
    {
        return $this->google;
    }


    public function toFront() {
        $a = get_object_vars($this);
        $a['main_address'] = nl2br($a['main_address']);
        unset($a['id']);
        return $a;
    }

    /**
     * Set main_address_name
     *
     * @param string $mainAddressName
     * @return Contact
     */
    public function setMainAddressName($mainAddressName)
    {
        $this->main_address_name = $mainAddressName;

        return $this;
    }

    /**
     * Get main_address_name
     *
     * @return string 
     */
    public function getMainAddressName()
    {
        return $this->main_address_name;
    }

    /**
     * Set main_address
     *
     * @param string $mainAddress
     * @return Contact
     */
    public function setMainAddress($mainAddress)
    {
        $this->main_address = $mainAddress;

        return $this;
    }

    /**
     * Get main_address
     *
     * @return string 
     */
    public function getMainAddress()
    {
        return $this->main_address;
    }

    /**
     * Set main_phone
     *
     * @param string $mainPhone
     * @return Contact
     */
    public function setMainPhone($mainPhone)
    {
        $this->main_phone = $mainPhone;

        return $this;
    }

    /**
     * Get main_phone
     *
     * @return string 
     */
    public function getMainPhone()
    {
        return $this->main_phone;
    }

    /**
     * Set main_fax
     *
     * @param string $mainFax
     * @return Contact
     */
    public function setMainFax($mainFax)
    {
        $this->main_fax = $mainFax;

        return $this;
    }

    /**
     * Get main_fax
     *
     * @return string 
     */
    public function getMainFax()
    {
        return $this->main_fax;
    }

    /**
     * Set main_email
     *
     * @param string $mainEmail
     * @return Contact
     */
    public function setMainEmail($mainEmail)
    {
        $this->main_email = $mainEmail;

        return $this;
    }

    /**
     * Get main_email
     *
     * @return string 
     */
    public function getMainEmail()
    {
        return $this->main_email;
    }

    /**
     * Set first_service_name
     *
     * @param string $firstServiceName
     * @return Contact
     */
    public function setFirstServiceName($firstServiceName)
    {
        $this->first_service_name = $firstServiceName;

        return $this;
    }

    /**
     * Get first_service_name
     *
     * @return string 
     */
    public function getFirstServiceName()
    {
        return $this->first_service_name;
    }

    /**
     * Set first_service_phone
     *
     * @param string $firstServicePhone
     * @return Contact
     */
    public function setFirstServicePhone($firstServicePhone)
    {
        $this->first_service_phone = $firstServicePhone;

        return $this;
    }

    /**
     * Get first_service_phone
     *
     * @return string 
     */
    public function getFirstServicePhone()
    {
        return $this->first_service_phone;
    }

    /**
     * Set first_service_resp
     *
     * @param string $firstServiceResp
     * @return Contact
     */
    public function setFirstServiceResp($firstServiceResp)
    {
        $this->first_service_resp = $firstServiceResp;

        return $this;
    }

    /**
     * Get first_service_resp
     *
     * @return string 
     */
    public function getFirstServiceResp()
    {
        return $this->first_service_resp;
    }

    /**
     * Set second_service_name
     *
     * @param string $secondServiceName
     * @return Contact
     */
    public function setSecondServiceName($secondServiceName)
    {
        $this->second_service_name = $secondServiceName;

        return $this;
    }

    /**
     * Get second_service_name
     *
     * @return string 
     */
    public function getSecondServiceName()
    {
        return $this->second_service_name;
    }

    /**
     * Set second_service_phone
     *
     * @param string $secondServicePhone
     * @return Contact
     */
    public function setSecondServicePhone($secondServicePhone)
    {
        $this->second_service_phone = $secondServicePhone;

        return $this;
    }

    /**
     * Get second_service_phone
     *
     * @return string 
     */
    public function getSecondServicePhone()
    {
        return $this->second_service_phone;
    }

    /**
     * Set second_service_resp
     *
     * @param string $secondServiceResp
     * @return Contact
     */
    public function setSecondServiceResp($secondServiceResp)
    {
        $this->second_service_resp = $secondServiceResp;

        return $this;
    }

    /**
     * Get second_service_resp
     *
     * @return string 
     */
    public function getSecondServiceResp()
    {
        return $this->second_service_resp;
    }

    /**
     * Set third_service_name
     *
     * @param string $thirdServiceName
     * @return Contact
     */
    public function setThirdServiceName($thirdServiceName)
    {
        $this->third_service_name = $thirdServiceName;

        return $this;
    }

    /**
     * Get third_service_name
     *
     * @return string 
     */
    public function getThirdServiceName()
    {
        return $this->third_service_name;
    }

    /**
     * Set third_service_phone
     *
     * @param string $thirdServicePhone
     * @return Contact
     */
    public function setThirdServicePhone($thirdServicePhone)
    {
        $this->third_service_phone = $thirdServicePhone;

        return $this;
    }

    /**
     * Get third_service_phone
     *
     * @return string 
     */
    public function getThirdServicePhone()
    {
        return $this->third_service_phone;
    }

    /**
     * Set third_service_resp
     *
     * @param string $thirdServiceResp
     * @return Contact
     */
    public function setThirdServiceResp($thirdServiceResp)
    {
        $this->third_service_resp = $thirdServiceResp;

        return $this;
    }

    /**
     * Get third_service_resp
     *
     * @return string 
     */
    public function getThirdServiceResp()
    {
        return $this->third_service_resp;
    }

    /**
     * Set fourth_service_name
     *
     * @param string $fourthServiceName
     * @return Contact
     */
    public function setFourthServiceName($fourthServiceName)
    {
        $this->fourth_service_name = $fourthServiceName;

        return $this;
    }

    /**
     * Get fourth_service_name
     *
     * @return string 
     */
    public function getFourthServiceName()
    {
        return $this->fourth_service_name;
    }

    /**
     * Set fourth_service_phone
     *
     * @param string $fourthServicePhone
     * @return Contact
     */
    public function setFourthServicePhone($fourthServicePhone)
    {
        $this->fourth_service_phone = $fourthServicePhone;

        return $this;
    }

    /**
     * Get fourth_service_phone
     *
     * @return string 
     */
    public function getFourthServicePhone()
    {
        return $this->fourth_service_phone;
    }

    /**
     * Set fourth_service_resp
     *
     * @param string $fourthServiceResp
     * @return Contact
     */
    public function setFourthServiceResp($fourthServiceResp)
    {
        $this->fourth_service_resp = $fourthServiceResp;

        return $this;
    }

    /**
     * Get fourth_service_resp
     *
     * @return string 
     */
    public function getFourthServiceResp()
    {
        return $this->fourth_service_resp;
    }
}
