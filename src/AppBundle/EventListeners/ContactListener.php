<?php

namespace AppBundle\EventListeners;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

use AppBundle\Entity\Contact;


class ContactListener {

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $entities = $this->em->getRepository('AppBundle:Contact')->findAll();
        $entity = empty($entities) ? new Contact() : $entities[0] ;

        $event->getRequest()->attributes->set('contacts', $entity->toFront());
    }
}
