<?php

namespace AppBundle\EventListeners;

use Ines\Bundle\CoreBundle\ContentType\ContentTypeManager;
use Ines\Bundle\CoreBundle\Entity\ContentType;
use Ines\Bundle\CoreBundle\Menu\AdminMenuBuilder;
use Ines\Bundle\CoreBundle\Menu\Event\ConfigureAdminMenuEvent;
use Knp\Menu\ItemInterface;
use Symfony\Component\Translation\TranslatorInterface;


class AdminMenuListener
{
    protected $manager;

    protected $translator;

    public function __construct(ContentTypeManager $manager, TranslatorInterface $translator)
    {
        $this->manager = $manager;
        $this->translator = $translator;
    }

    public function onPreCreate(ConfigureAdminMenuEvent $event)
    {
        $factory = $event->getFactory();
        $menu = $event->getMenu();
        $authorizationChecker = $event->getAuthorizationChecker();

        $tpl = AdminMenuBuilder::TPL;

        // remove menu elements
        unset($menu['taxonomy']);
        unset($menu['comment']);
        

        // menu element
        if ($authorizationChecker->isGranted('ROLE_CONTACT')) {
            $label = $this->translator
                ->trans('nav.contact', array(), 'InesCoreBundle');
            $menu->addChild('contact', array(
                'route'      => 'site_admin_contact',
                'attributes' => array('class' => 'nav-item'),
                'label'      => sprintf($tpl, 'fa fa-phone', $label),
                'extras'     => array('safe_label' => true),
            ));
        }

        if ($authorizationChecker->isGranted('ROLE_STORAGE_MANAGER')) {
            $label = $this->translator
                     ->trans('nav.storage', array(), 'InesCoreBundle');

            $menu->addChild('storage', array(
                'route'      => 'site_admin_storage',
                'attributes' => array('class' => 'nav-item'),
                'label'      => sprintf($tpl, 'fa fa-cloud-download', $label),
                'extras'     => array('safe_label' => true),
            ));
         }
    }

    protected function addChildItem(ItemInterface $item, ContentType $contentType)
    {
        $item->addChild($contentType->getSlug(), array(
            'label'           => $this->translator->trans($contentType->getName(), array(), 'InesCoreBundle'),
            'route'           => 'ines_core_content_index',
            'routeParameters' => array('contentTypeSlug' => $contentType->getSlug()),
        ));
    }
}
