<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150617120903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ines_contents (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, published_at DATETIME NOT NULL, expired_at DATETIME DEFAULT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, excerpt LONGTEXT DEFAULT NULL, body LONGTEXT DEFAULT NULL, status VARCHAR(64) NOT NULL, content_type VARCHAR(128) NOT NULL, language VARCHAR(5) NOT NULL, parent INT NOT NULL, position INT NOT NULL, comment_enabled TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_7BF6D7AF675F31B (author_id), INDEX slug_idx (language, content_type, slug, status), INDEX status_idx (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_content_terms (content_id INT NOT NULL, term_id INT NOT NULL, INDEX IDX_3A1026684A0A3ED (content_id), INDEX IDX_3A10266E2C35FC (term_id), PRIMARY KEY(content_id, term_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_content_metas (id INT AUTO_INCREMENT NOT NULL, content_id INT DEFAULT NULL, format VARCHAR(64) NOT NULL, field_group VARCHAR(128) DEFAULT NULL, meta_key VARCHAR(128) NOT NULL, meta_value LONGTEXT DEFAULT NULL, INDEX IDX_C669C42B84A0A3ED (content_id), INDEX meta_key_idx (meta_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_content_types (id INT AUTO_INCREMENT NOT NULL, embedded_form_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, link_rewrite VARCHAR(128) NOT NULL, supports LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', has_admin_view TINYINT(1) NOT NULL, has_list_view TINYINT(1) NOT NULL, has_single_view TINYINT(1) NOT NULL, has_feed_view TINYINT(1) NOT NULL, has_archive_view TINYINT(1) NOT NULL, has_comment_view TINYINT(1) NOT NULL, is_efg_search TINYINT(1) DEFAULT \'0\' NOT NULL, max_per_page INT DEFAULT 10 NOT NULL, seo_title VARCHAR(500) DEFAULT NULL, seo_description VARCHAR(500) DEFAULT NULL, UNIQUE INDEX UNIQ_D233B427989D9B62 (slug), UNIQUE INDEX UNIQ_D233B427788D5722 (link_rewrite), UNIQUE INDEX UNIQ_D233B427DC7806DA (embedded_form_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_comments (id INT AUTO_INCREMENT NOT NULL, content_id INT DEFAULT NULL, username VARCHAR(128) NOT NULL, email VARCHAR(128) NOT NULL, ip_address INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, body LONGTEXT NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_ECDBEA2784A0A3ED (content_id), INDEX status_idx (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_field_configs (id INT AUTO_INCREMENT NOT NULL, field_group_id INT DEFAULT NULL, position INT NOT NULL, `label` VARCHAR(128) NOT NULL, name VARCHAR(128) NOT NULL, field_type VARCHAR(64) NOT NULL, is_required TINYINT(1) NOT NULL, min_length INT DEFAULT NULL, max_length INT DEFAULT NULL, choices LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', display_size VARCHAR(12) NOT NULL, help_text LONGTEXT DEFAULT NULL, default_value VARCHAR(255) DEFAULT NULL, is_multiple TINYINT(1) NOT NULL, content_type_source VARCHAR(128) DEFAULT NULL, options LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_CC8D3456286C5E8A (field_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_widgets (id INT AUTO_INCREMENT NOT NULL, area VARCHAR(128) NOT NULL, type VARCHAR(55) NOT NULL, language VARCHAR(5) NOT NULL, position INT DEFAULT 0 NOT NULL, options LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_medias (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, filename VARCHAR(128) NOT NULL, author INT DEFAULT NULL, legend LONGTEXT DEFAULT NULL, alt_text VARCHAR(255) DEFAULT NULL, mime_type VARCHAR(64) NOT NULL, width INT DEFAULT NULL, height INT DEFAULT NULL, filesize NUMERIC(10, 0) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_nodes (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, alias VARCHAR(128) DEFAULT NULL, root INT DEFAULT NULL, lft INT NOT NULL, rgt INT NOT NULL, lvl INT NOT NULL, url VARCHAR(128) DEFAULT NULL, ref_class VARCHAR(128) DEFAULT NULL, ref_id INT DEFAULT NULL, max_lvl INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, attributes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', route_name VARCHAR(128) DEFAULT NULL, route_parameters LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', language VARCHAR(5) NOT NULL, INDEX IDX_39953150727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_metas (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, value LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, expired_at DATETIME DEFAULT NULL, INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_embedded_forms (id INT AUTO_INCREMENT NOT NULL, css_class VARCHAR(128) DEFAULT NULL, has_gvalidation TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_field_groups (id INT AUTO_INCREMENT NOT NULL, embedded_form_id INT DEFAULT NULL, position INT NOT NULL, may_be_repeated TINYINT(1) NOT NULL, may_be_sorted TINYINT(1) NOT NULL, display_position VARCHAR(12) NOT NULL, display_pane TINYINT(1) NOT NULL, legend VARCHAR(128) NOT NULL, name VARCHAR(128) NOT NULL, INDEX IDX_AACEA5EDDC7806DA (embedded_form_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_terms (id INT AUTO_INCREMENT NOT NULL, taxonomy_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, language VARCHAR(5) NOT NULL, INDEX IDX_AC0A0BDD9557E6F6 (taxonomy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_taxonomies (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, supports LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_302B914592FC23A8 (username_canonical), UNIQUE INDEX UNIQ_302B9145A0D96FBF (email_canonical), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_web_forms (id INT AUTO_INCREMENT NOT NULL, embedded_form_id INT DEFAULT NULL, name VARCHAR(128) NOT NULL, slug VARCHAR(128) NOT NULL, title VARCHAR(128) DEFAULT NULL, description LONGTEXT DEFAULT NULL, submit_text VARCHAR(128) DEFAULT NULL, limit_entries INT DEFAULT NULL, is_active TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, started_at DATETIME DEFAULT NULL, ended_at DATETIME DEFAULT NULL, active_notifs TINYINT(1) NOT NULL, notifications LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_4E2E26A8989D9B62 (slug), UNIQUE INDEX UNIQ_4E2E26A8DC7806DA (embedded_form_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ines_translations (content_id INT NOT NULL, language VARCHAR(5) NOT NULL, translation_id INT NOT NULL, UNIQUE INDEX translation_idx (translation_id), PRIMARY KEY(content_id, language)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ines_contents ADD CONSTRAINT FK_7BF6D7AF675F31B FOREIGN KEY (author_id) REFERENCES ines_users (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ines_content_terms ADD CONSTRAINT FK_3A1026684A0A3ED FOREIGN KEY (content_id) REFERENCES ines_contents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_content_terms ADD CONSTRAINT FK_3A10266E2C35FC FOREIGN KEY (term_id) REFERENCES ines_terms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_content_metas ADD CONSTRAINT FK_C669C42B84A0A3ED FOREIGN KEY (content_id) REFERENCES ines_contents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_content_types ADD CONSTRAINT FK_D233B427DC7806DA FOREIGN KEY (embedded_form_id) REFERENCES ines_embedded_forms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_comments ADD CONSTRAINT FK_ECDBEA2784A0A3ED FOREIGN KEY (content_id) REFERENCES ines_contents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_field_configs ADD CONSTRAINT FK_CC8D3456286C5E8A FOREIGN KEY (field_group_id) REFERENCES ines_field_groups (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_nodes ADD CONSTRAINT FK_39953150727ACA70 FOREIGN KEY (parent_id) REFERENCES ines_nodes (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE ines_field_groups ADD CONSTRAINT FK_AACEA5EDDC7806DA FOREIGN KEY (embedded_form_id) REFERENCES ines_embedded_forms (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_terms ADD CONSTRAINT FK_AC0A0BDD9557E6F6 FOREIGN KEY (taxonomy_id) REFERENCES ines_taxonomies (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ines_web_forms ADD CONSTRAINT FK_4E2E26A8DC7806DA FOREIGN KEY (embedded_form_id) REFERENCES ines_embedded_forms (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ines_content_terms DROP FOREIGN KEY FK_3A1026684A0A3ED');
        $this->addSql('ALTER TABLE ines_content_metas DROP FOREIGN KEY FK_C669C42B84A0A3ED');
        $this->addSql('ALTER TABLE ines_comments DROP FOREIGN KEY FK_ECDBEA2784A0A3ED');
        $this->addSql('ALTER TABLE ines_nodes DROP FOREIGN KEY FK_39953150727ACA70');
        $this->addSql('ALTER TABLE ines_content_types DROP FOREIGN KEY FK_D233B427DC7806DA');
        $this->addSql('ALTER TABLE ines_field_groups DROP FOREIGN KEY FK_AACEA5EDDC7806DA');
        $this->addSql('ALTER TABLE ines_web_forms DROP FOREIGN KEY FK_4E2E26A8DC7806DA');
        $this->addSql('ALTER TABLE ines_field_configs DROP FOREIGN KEY FK_CC8D3456286C5E8A');
        $this->addSql('ALTER TABLE ines_content_terms DROP FOREIGN KEY FK_3A10266E2C35FC');
        $this->addSql('ALTER TABLE ines_terms DROP FOREIGN KEY FK_AC0A0BDD9557E6F6');
        $this->addSql('ALTER TABLE ines_contents DROP FOREIGN KEY FK_7BF6D7AF675F31B');
        $this->addSql('DROP TABLE ines_contents');
        $this->addSql('DROP TABLE ines_content_terms');
        $this->addSql('DROP TABLE ines_content_metas');
        $this->addSql('DROP TABLE ines_content_types');
        $this->addSql('DROP TABLE ines_comments');
        $this->addSql('DROP TABLE ines_field_configs');
        $this->addSql('DROP TABLE ines_widgets');
        $this->addSql('DROP TABLE ines_medias');
        $this->addSql('DROP TABLE ines_nodes');
        $this->addSql('DROP TABLE ines_metas');
        $this->addSql('DROP TABLE ines_embedded_forms');
        $this->addSql('DROP TABLE ines_field_groups');
        $this->addSql('DROP TABLE ines_terms');
        $this->addSql('DROP TABLE ines_taxonomies');
        $this->addSql('DROP TABLE ines_users');
        $this->addSql('DROP TABLE ines_web_forms');
        $this->addSql('DROP TABLE ines_translations');
    }
}
